package ru.ohrana_neva.client.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.skydoves.colorpickerpreference.ColorEnvelope;
import com.skydoves.colorpickerpreference.ColorListener;
import com.skydoves.colorpickerpreference.ColorPickerDialog;

import ru.ohrana_neva.client.R;
import ru.ohrana_neva.client.SharedPreferencesHelper;
import ru.ohrana_neva.client.Utils;
import ru.ohrana_neva.client.data.ApiClient;
import ru.ohrana_neva.client.sounds.PlaySound;

public class SettingsFragment extends BaseDialogFragment {
    public Dialog prepareDialog(final Context context, final int layoutId) {
        final Dialog dialog = super.prepareDialog(context, layoutId);

        Button btnBg = (Button) dialog.findViewById(R.id.btn_bg_color);
        btnBg.setOnClickListener(v -> {
            showColorPickerDialog((Button) v);
        });
        Button btnSosBg = (Button) dialog.findViewById(R.id.btn_sos_bg_color);
        btnSosBg.setOnClickListener(v -> {
            showColorPickerDialog((Button) v);
        });
        Button btnRoute = (Button) dialog.findViewById(R.id.btn_route_color);
        btnRoute.setOnClickListener(v -> {
            showColorPickerDialog((Button) v);
        });
        Button btnSound = (Button) dialog.findViewById(R.id.btn_sound);
        btnSound.setOnClickListener(v -> {
            PlaySound.getInstance(v.getContext()).openGalleryForAudio();
        });

        EditText etPhone = (EditText) dialog.findViewById(R.id.et_phone);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            etPhone.addTextChangedListener(new PhoneNumberFormattingTextWatcher("RU"));
        } else {
            etPhone.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
        }
        etPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (etPhone.getText().length() > 0) {
                    SharedPreferencesHelper.setOperatorPhone(etPhone.getContext(), etPhone.getText().toString().trim().replace(" ", "").replace("-", ""));
                }
            }
        });

        return dialog;
    }

    @Override
    protected void prepareButtons(View view) {
        Button btnBg = (Button) view.findViewById(R.id.btn_bg_color);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            btnBg.setBackgroundResource(R.drawable.btn_v21_no_corners);
            btnBg.setStateListAnimator(null);
        } else {
            btnBg.setBackgroundResource(R.drawable.btn_rounded_no_corners);
        }
        Button btnSosBg = (Button) view.findViewById(R.id.btn_sos_bg_color);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            btnSosBg.setBackgroundResource(R.drawable.btn_v21_red_no_corners);
            btnSosBg.setStateListAnimator(null);
        } else {
            btnSosBg.setBackgroundResource(R.drawable.btn_rounded_red_no_corners);
        }
        Button btnRoute = (Button) view.findViewById(R.id.btn_route_color);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            btnRoute.setBackgroundResource(R.drawable.btn_v21_large_radius);
            btnRoute.setStateListAnimator(null);
        } else {
            btnRoute.setBackgroundResource(R.drawable.btn_rounded_large_radius);
        }
        Button btnSound = (Button) view.findViewById(R.id.btn_sound);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            btnSound.setBackgroundResource(R.drawable.btn_v21_large_radius);
            btnSound.setStateListAnimator(null);
        } else {
            btnSound.setBackgroundResource(R.drawable.btn_rounded_large_radius);
        }

        Utils.setButtonColors(btnBg, SharedPreferencesHelper.getButtonsBackgroundColor(btnBg.getContext()), Utils.getHighlightedColor(SharedPreferencesHelper.getButtonsBackgroundColor(btnBg.getContext())));
        Utils.setButtonColors(btnSosBg, SharedPreferencesHelper.getSOSButtonBackgroundColor(btnSosBg.getContext()), Utils.getHighlightedColor(SharedPreferencesHelper.getSOSButtonBackgroundColor(btnSosBg.getContext())));
        Utils.setButtonColors(btnRoute, SharedPreferencesHelper.getRouteColor(btnRoute.getContext()), Utils.getHighlightedColor(SharedPreferencesHelper.getRouteColor(btnRoute.getContext())));
    }

    @Override
    protected void changeButtonColors(View view, int color) {
        Button btnBg = (Button) view.findViewById(R.id.btn_bg_color);
        Utils.setButtonColors(btnBg, color, Utils.getHighlightedColor(color));
    }

    @Override
    protected void changeSosButtonColor(View view, int color) {
        Button btnSosBg = (Button) view.findViewById(R.id.btn_sos_bg_color);
        Utils.setButtonColors(btnSosBg, color, Utils.getHighlightedColor(color));
    }

    private void showColorPickerDialog(Button button) {
        if (contextRef == null || contextRef.get() == null) return;
        ColorPickerDialog.Builder builder = new ColorPickerDialog.Builder(contextRef.get(), AlertDialog.THEME_DEVICE_DEFAULT_DARK);
        builder.setPositiveButton(contextRef.get().getResources().getString(R.string.str_ok), colorEnvelope -> {
            if (contextRef == null || contextRef.get() == null || colorEnvelope == null) return;
            switch (button.getId()) {
                case R.id.btn_bg_color: {
                    SharedPreferencesHelper.setButtonsBackgroundColor(contextRef.get(), colorEnvelope.getColor());
                    ApiClient.getInstance(contextRef.get()).getBgColorSubject().onNext(colorEnvelope.getColor());
                }; break;
                case R.id.btn_sos_bg_color: {
                    SharedPreferencesHelper.setSOSButtonBackgroundColor(contextRef.get(), colorEnvelope.getColor());
                    ApiClient.getInstance(contextRef.get()).getSosBgColorSubject().onNext(colorEnvelope.getColor());
                }; break;
                case R.id.btn_route_color: {
                    SharedPreferencesHelper.setRouteColor(contextRef.get(), colorEnvelope.getColor());
                    ApiClient.getInstance(contextRef.get()).getRouteColorSubject().onNext(colorEnvelope.getColor());
                }; break;
            }
        });
        builder.setNegativeButton(contextRef.get().getResources().getString(R.string.cancel_btn), (dialogInterface, i) -> {
            dialogInterface.dismiss();
        });
        builder.show();
    }

    public void showDialog(Context context) {
        final Dialog dialog = prepareDialog(context, R.layout.dialogfragment_settings);
        dialog.show();
    }

    @Override
    public void refreshOnConfigChanged(Context context) {
        super.refreshOnConfigChanged(context);
        showDialog(context);
    }
}
