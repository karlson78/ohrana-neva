package ru.ohrana_neva.client.data.model;

import android.os.Parcel;
import android.text.TextUtils;

import ru.ohrana_neva.client.Consts;

public class AlarmMessage extends Message {
    public double lat;
    public double lon;
    public String id;
    public String address;
    public String name;
    public String number;
    public String pult_id;
    public int type;

    public AlarmMessage() {}

    protected AlarmMessage(Parcel in) {
        action = in.readString();
        lat = in.readDouble();
        lon = in.readDouble();
        id = in.readString();
        address = in.readString();
        name = in.readString();
        number = in.readString();
        pult_id = in.readString();
        type = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(action);
        dest.writeDouble(lat);
        dest.writeDouble(lon);
        dest.writeString(id);
        dest.writeString(address);
        dest.writeString(name);
        dest.writeString(number);
        dest.writeString(pult_id);
        dest.writeInt(type);
    }

    public static final Creator<AlarmMessage> CREATOR = new Creator<AlarmMessage>() {
        @Override
        public AlarmMessage createFromParcel(Parcel in) {
            return new AlarmMessage(in);
        }

        @Override
        public AlarmMessage[] newArray(int size) {
            return new AlarmMessage[size];
        }
    };

    @Override
    public String getHeader() {
        return (TextUtils.isEmpty(this.name) ? "" : this.name);
    }

    @Override
    public String getText() {
        String addr = this.address;
        if (this.pult_id != null) {
            addr = addr + ", пультовый номер " + this.pult_id;
        } else
        if (this.number != null) {
            addr = addr + ", пультовый номер " + this.number;
        }
        String typeString = "";
        switch (this.type) {
            case Consts.TYPE_SECURITY:
                typeString = "ОС";
                break;
            case Consts.TYPE_FIRE:
                typeString = "ПС";
                break;
            case Consts.TYPE_ALARM_BUTTON:
                typeString = "КТС";
                break;
        }
        if (!TextUtils.isEmpty(typeString)) {
            addr = addr + ", тип: " + typeString;
        }
        return addr;
    }
}
