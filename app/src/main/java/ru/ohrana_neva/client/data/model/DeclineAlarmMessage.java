package ru.ohrana_neva.client.data.model;

import android.os.Parcel;

public class DeclineAlarmMessage extends Message {
    public String message;

    public DeclineAlarmMessage() {}

    protected DeclineAlarmMessage(Parcel in) {
        action = in.readString();
        message = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(action);
        dest.writeString(message);
    }

    public static final Creator<DeclineAlarmMessage> CREATOR = new Creator<DeclineAlarmMessage>() {
        @Override
        public DeclineAlarmMessage createFromParcel(Parcel in) {
            return new DeclineAlarmMessage(in);
        }

        @Override
        public DeclineAlarmMessage[] newArray(int size) {
            return new DeclineAlarmMessage[size];
        }
    };
}
