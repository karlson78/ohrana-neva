package ru.ohrana_neva.client.data;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.ArrayList;

import ru.ohrana_neva.client.MainActivity;
import ru.ohrana_neva.client.R;
import ru.ohrana_neva.client.data.model.AlarmMessage;
import ru.ohrana_neva.client.data.model.DeclineAlarmMessage;
import ru.ohrana_neva.client.data.model.IsFreeMessage;
import ru.ohrana_neva.client.data.model.Message;
import ru.ohrana_neva.client.data.model.SosMessage;

public class Messages {
    public static int NOTIFY_ID;

    public static Message convertSosMessage(JsonObject jsonObj) {
        String action = jsonObj.get("action").getAsString().toUpperCase();
        if (action.compareTo("ISFREE") == 0) {
            JsonObject jsonMessage = jsonObj.get("message").getAsJsonObject();
            IsFreeMessage message = new IsFreeMessage();
            message.action = "isFree";
            boolean flag = jsonMessage.get("result").getAsBoolean();
            if (flag) {
                message.message = 1;
            } else {
                message.message = 0;
            }
            return message;
        }
        if (action.compareTo("DECLINEBRIGADE") == 0) {
            DeclineAlarmMessage message = new DeclineAlarmMessage();
            message.action = "declineBrigade";
            message.message = jsonObj.get("message").getAsString();
            return message;
        }
        JsonObject jsonMessage = jsonObj.get("message").getAsJsonObject();
        if (jsonMessage != null) {
            if (action.compareTo("SOS") == 0) {
                SosMessage sosMessage = new SosMessage();
                sosMessage.action = "sos";
                try {
                    if (jsonMessage.get("lat") != null) {
                        sosMessage.lat = jsonMessage.get("lat").getAsDouble();
                    }
                    if (jsonMessage.get("lon") != null) {
                        sosMessage.lon = jsonMessage.get("lon").getAsDouble();
                    }
                    if (jsonMessage.get("id") != null) {
                        sosMessage.id = jsonMessage.get("id").getAsString();
                    }
                    if (jsonMessage.get("address") != null) {
                        sosMessage.address = jsonMessage.get("address").getAsString();
                    }
                } catch (Exception e) {}

                return sosMessage;
            } else
            if (action.compareTo("ALARM") == 0) {
                AlarmMessage alarmMessage = new AlarmMessage();
                alarmMessage.action = "alarm";
                try {
                    if (jsonMessage.get("type") != null) {
                        alarmMessage.type = jsonMessage.get("type").getAsInt();
                    }
                    if (jsonMessage.get("lat") != null) {
                        alarmMessage.lat = jsonMessage.get("lat").getAsDouble();
                    }
                    if (jsonMessage.get("lon") != null) {
                        alarmMessage.lon = jsonMessage.get("lon").getAsDouble();
                    }
                    if (jsonMessage.get("id") != null) {
                        alarmMessage.id = jsonMessage.get("id").getAsString();
                    }
                    if (jsonMessage.get("address") != null) {
                        alarmMessage.address = jsonMessage.get("address").getAsString();
                    }
                    if (jsonMessage.get("name") != null) {
                        alarmMessage.name = jsonMessage.get("name").getAsString();
                    }
                    if (jsonMessage.get("number") != null) {
                        alarmMessage.number = jsonMessage.get("number").getAsString();
                    }
                    if (jsonMessage.get("pult_id") != null) {
                        alarmMessage.pult_id = jsonMessage.get("pult_id").getAsString();
                    }
                } catch (Exception e) {}

                return alarmMessage;
            } else return null;
        } else {
            return null;
        }
    }

    public static void sendNotification(Message msg, Context context) {
        NOTIFY_ID++;
        Intent notificationIntent = new Intent(context, MainActivity.class);
        if (msg instanceof AlarmMessage) {
            notificationIntent.putExtra("action", "alarm");
            notificationIntent.putExtra("message", (AlarmMessage) msg);
        } else
        if (msg instanceof SosMessage) {
            notificationIntent.putExtra("action", "sos");
            notificationIntent.putExtra("message", (SosMessage) msg);
        }
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent contentIntent = PendingIntent.getActivity(context,
                0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);

        builder.setContentIntent(contentIntent)
                .setSmallIcon(R.drawable.notification_icon)
                .setContentTitle(msg.getHeader())
                .setContentText(msg.getText()) // Текст уведомления
                .setTicker(msg.getText())
                .setDefaults(NotificationCompat.DEFAULT_VIBRATE)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setWhen(System.currentTimeMillis())
                .setAutoCancel(true); // автоматически закрыть уведомление после нажатия

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.notify(NOTIFY_ID, builder.build());
    }

}
