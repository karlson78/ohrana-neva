package ru.ohrana_neva.client.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CityGuideRouteResponse {
    @SerializedName("features")
    public List<Feature> features;

    public static class Feature {
        @SerializedName("type")
        public String type;
        @SerializedName("geometry")
        public GeometryFeature geometry;
    }

    public static class GeometryFeature {
        @SerializedName("type")
        public String type;
        @SerializedName("coordinates")
        public List<List<Double>> coordinates;
    }

    public static class Properties {
        @SerializedName("totalTime")
        public int totalTime;
        @SerializedName("totalDistance")
        public int totalDistance;
        @SerializedName("result")
        public String result;
    }
}
