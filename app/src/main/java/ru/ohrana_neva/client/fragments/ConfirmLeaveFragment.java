package ru.ohrana_neva.client.fragments;

import android.app.Dialog;
import android.content.Context;
import android.widget.Button;

import ru.ohrana_neva.client.R;
import ru.ohrana_neva.client.data.ApiClient;

public class ConfirmLeaveFragment extends BaseDialogFragment {
    private Button btnOk;

    public Dialog prepareDialog(final Context context, final int layoutId) {
        final Dialog dialog = super.prepareDialog(context, layoutId);

        btnOk = (Button) dialog.findViewById(R.id.btn_ok);
        btnOk.setOnClickListener(v -> {
            ApiClient.getInstance(context).dialogEvent(layoutId, v.getId(), null);
            dialog.dismiss();
        });
        btnOk.setEnabled(ApiClient.getInstance(context).getLastIsFree() != null);

        return dialog;
    }

    public void enableOK() {
        btnOk.setEnabled(true);
    }

    public void showDialog(Context context) {
        final Dialog dialog = prepareDialog(context, R.layout.dialogfragment_confirm_leave);
        dialog.show();
    }

    @Override
    public void refreshOnConfigChanged(Context context) {
        super.refreshOnConfigChanged(context);
        showDialog(context);
    }
}
