package ru.ohrana_neva.client.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.ohrana_neva.client.R;
import ru.ohrana_neva.client.data.model.ActionRecord;

public class ActionsAdapter extends RecyclerView.Adapter<ActionsAdapter.VH> {
    private List<ActionRecord> data = null;
    private WeakReference<Context> contextRef = null;

    public void setData(List<ActionRecord> data) {
        if (this.data != null) {
            this.data.clear();
        } else {
            this.data = new ArrayList<>();
        }
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public ActionsAdapter.VH onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (contextRef == null || contextRef.get() == null) {
            contextRef = new WeakReference<Context>(parent.getContext());
        }
        View itemView = inflater.inflate(R.layout.action_item, parent, false);
        return new ActionsAdapter.VH(itemView);
    }

    @Override
    public void onBindViewHolder(ActionsAdapter.VH holder, int position) {
        holder.bind(data.get(position));
    }

    @Override
    public int getItemCount() {
        return (data == null) ? 0 : data.size();
    }

    public class VH extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_alarm_time)
        TextView tvAlarmTime;
        @BindView(R.id.tv_alarm_user)
        TextView tvAlarmUser;
        @BindView(R.id.tv_alarm_text)
        TextView tvAlarmText;

        public VH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(ActionRecord rec) {
            SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
            Date date = new Date();
            date.setTime(rec.created_timestamp*1000);
            tvAlarmTime.setText(format.format(date));
            tvAlarmUser.setText(rec.user.username);
            tvAlarmText.setText(rec.text);
        }
    }
}
