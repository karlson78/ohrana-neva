package ru.ohrana_neva.client.fragments;

import android.app.Dialog;
import android.content.Context;

import ru.ohrana_neva.client.R;

public class ObjectInfoFragment extends BaseDialogFragment {
    public void showDialog(Context context) {
        final Dialog dialog = super.prepareDialog(context, R.layout.dialogfragment_object_info);
        dialog.show();
    }

    @Override
    public void refreshOnConfigChanged(Context context) {
        super.refreshOnConfigChanged(context);
        showDialog(context);
    }
}
