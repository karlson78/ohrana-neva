package ru.ohrana_neva.client;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Point;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Polyline;

import java.io.File;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.fabric.sdk.android.Fabric;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ru.ohrana_neva.client.adapters.ActionsAdapter;
import ru.ohrana_neva.client.adapters.JournalAdapter;
import ru.ohrana_neva.client.data.ApiClient;
import ru.ohrana_neva.client.data.LocationsHandler;
import ru.ohrana_neva.client.data.LocationsRecorder;
import ru.ohrana_neva.client.data.Messages;
import ru.ohrana_neva.client.data.model.ActionRecord;
import ru.ohrana_neva.client.data.model.AlarmApplyMessage;
import ru.ohrana_neva.client.data.model.AlarmMessage;
import ru.ohrana_neva.client.data.model.CityGuideRouteResponse;
import ru.ohrana_neva.client.data.model.DeclineAlarmMessage;
import ru.ohrana_neva.client.data.model.IsFreeMessage;
import ru.ohrana_neva.client.data.model.JournalRecord;
import ru.ohrana_neva.client.data.model.Message;
import ru.ohrana_neva.client.data.model.SosApplyMessage;
import ru.ohrana_neva.client.data.model.SosMessage;
import ru.ohrana_neva.client.fragments.AcceptSosFragment;
import ru.ohrana_neva.client.fragments.AlarmDeclineReasonFragment;
import ru.ohrana_neva.client.fragments.BaseDialogFragment;
import ru.ohrana_neva.client.fragments.ConfirmLeaveFragment;
import ru.ohrana_neva.client.fragments.DownloadProgressFragment;
import ru.ohrana_neva.client.fragments.LoginFragment;
import ru.ohrana_neva.client.fragments.ReportFragment;
import ru.ohrana_neva.client.fragments.SettingsFragment;
import ru.ohrana_neva.client.sockets.SocketListener;
import ru.ohrana_neva.client.sounds.PlaySound;
import ru.ohrana_neva.client.versionupdater.DownloadProgressListener;
import ru.ohrana_neva.client.versionupdater.VersionUpdater;
import ru.ohrana_neva.client.views.SimpleDividerItemDecoration;
import ru.ohrana_neva.client.views.TouchableWrapper;

public class MainActivity extends AppCompatActivity implements TouchableWrapper.UpdateMapAfterUserInteraction, LocationsHandler.OnGoogleApiConnectedListener {
    @BindView(R.id.wrapper_layout)
    TouchableWrapper wrapper;
    @BindView(R.id.mapview)
    MapView mapView;
    @BindView(R.id.btn_status)
    Button btnStatus;
    @BindView(R.id.btn_last_object)
    Button btnLastObject;
    @BindView(R.id.btn_log)
    Button btnLog;
    @BindView(R.id.btn_sos)
    Button btnSOS;
    @BindView(R.id.btn_settings)
    Button btnSettings;
    @BindView(R.id.btn_logout)
    Button btnLogout;
    @BindView(R.id.buttons_layout)
    LinearLayout llButtons;
    @BindView(R.id.spinner_layout)
    LinearLayout llSpinner;
    @BindView(R.id.spinner_status_edit)
    Spinner spinnerStatus;
    @BindView(R.id.ll_accepted_alarm_buttons)
    LinearLayout llAcceptedAlarmButtons;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.tv_object_name)
    TextView tvObjectName;
    @BindView(R.id.btn_arrived)
    Button btnArrived;
    @BindView(R.id.btn_hard_to_move)
    Button btnHardToMove;
    @BindView(R.id.btn_free)
    Button btnFree;
    @BindView(R.id.location_btn)
    Button btnLocation;
    @BindView(R.id.space1)
    View space1;
    @BindView(R.id.space2)
    View space2;
    @BindView(R.id.rv_journal)
    RecyclerView rvJournal;

    private static MainActivity instance = null;

    boolean currentLocationShown = true;

    private static volatile SocketListener socketListener = null;
    private Disposable dialogDisposable = null;
    private Disposable sosDisposable = null;
    private Disposable routeDisposable = null;
    private Disposable timeDisposable = null;
    private Disposable logDisposable = null;
    private Disposable bgColorDisposable = null;
    private Disposable sosBgColorDisposable = null;
    private Disposable routeColorDisposable = null;
    private Disposable getVersionDisposable = null;
    private CompositeDisposable compositeDisposable = null;
    private static Message prevMessage;

    private volatile int status;
    private boolean isPaused;
    private boolean isInBackground;
    private boolean isActionsPopupShown;

    private Polyline polylineMapObject = null;

    private long routeRequestTime = 0;
    private int rideDifficultLockRemainingTime = 0;

    private PopupWindow popupWindow = null;

    private Handler handler = new Handler();

    public static MainActivity getInstance() {
        return instance;
    }

    public int getStatus() {
        synchronized (this) {
            return status;
        }
    }

    public void setInBackground(boolean flag) {
        synchronized (this) {
            isInBackground = flag;
        }
    }

    public void setStatus(int status) {
        synchronized (this) {
            if (status != Consts.STATUS_RIDE && status != Consts.STATUS_RIDE_DIFFICULT) {
                if (polylineMapObject != null) {
                    mapView.getOverlayManager().remove(polylineMapObject);
                    polylineMapObject = null;
                    LocationsRecorder.getInstance().setGeometry(null);
                }
            }

            if (this.status == status) return;
            //clearOSMcache();
            this.status = status;
            setButtonsVisibility();
            btnLastObject.setEnabled(ApiClient.getInstance(this).getLastAlarmOrSos() != null);
        }
    }

    public void routeRequest(double latFrom, double lonFrom, double latTo, double lonTo) {
        if (compositeDisposable == null) return;
        if (routeDisposable != null) {
            compositeDisposable.remove(routeDisposable);
        }
        routeDisposable = ApiClient.getInstance(this).getRoute(latFrom, lonFrom, latTo, lonTo)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> handleRouteLoaded(response), t -> handleError(t));
        compositeDisposable.add(routeDisposable);
    }

    public void handleRouteLoaded(CityGuideRouteResponse response) {
        synchronized (this) {
            if (polylineMapObject != null) {
                mapView.getOverlayManager().remove(polylineMapObject);
                polylineMapObject = null;
                System.gc();
            }

            if (response != null && response.features != null && response.features.size() > 0 &&
                    (status == Consts.STATUS_RIDE || status == Consts.STATUS_RIDE_DIFFICULT))
            {
                LocationsRecorder.getInstance().setGeometry(response.features.get(0).geometry);
                Pair<List<GeoPoint>, LocationsRecorder.MapRect> result = LocationsRecorder.getInstance().getCurrentPolyline();
                if (result != null) {
                    addPolyline(result.first, result.second);
                }
            }
        }
    }

    private void addPolyline(List<GeoPoint> polyline, LocationsRecorder.MapRect rect) {
        //ArrayList<IGeoPoint> polylineCopy = new ArrayList<>();
        //polylineCopy.addAll(polyline);
/*
        polylineMapObject = new Polyline()
            .color(SharedPreferencesHelper.getRouteColor(this))
            .width(5.f)
            .points(polylineCopy);
*/
        polylineMapObject = new Polyline();
        polylineMapObject.setColor(SharedPreferencesHelper.getRouteColor(this));
        polylineMapObject.setWidth(5.f);
        polylineMapObject.setPoints(polyline);

        int zoom = mapView.getZoomLevel();
        if (currentLocationShown) {
            mapView.getController().setCenter(
                    new GeoPoint(LocationsRecorder.getInstance().getLastLocation().lat, LocationsRecorder.getInstance().getLastLocation().lon));
        }

        new GeoPoint(rect.minLat, rect.minLon);
        Point point = new Point();
        Point pointSW = mapView.getProjection().toPixels(new GeoPoint(rect.minLat, rect.minLon), point);
        int ptBottomLeftX = pointSW.x;
        int ptBottomLeftY = pointSW.y;
        Point pointNE = mapView.getProjection().toPixels(new GeoPoint(rect.maxLat, rect.maxLon), point);
        int ptTopRightX = pointNE.x;
        int ptTopRightY = pointNE.y;
        int diffX = Math.abs(ptTopRightX - ptBottomLeftX);
        int diffY = Math.abs(ptBottomLeftY - ptTopRightY);
        if (diffX > mapView.getWidth() || diffY > mapView.getHeight()) {
            while (diffX > mapView.getWidth() || diffY > mapView.getHeight()) {
                diffX /= 2;
                diffY /= 2;
                zoom -= 1;
            }
        } else {
            while (diffX <= mapView.getWidth() && diffY <= mapView.getHeight()) {
                diffX *= 2;
                diffY *= 2;
                zoom += 1;
            }
            zoom -= 1;
        }
        if (zoom > mapView.getMaxZoomLevel()) {
            zoom = mapView.getMaxZoomLevel();
        } else
        if (zoom < mapView.getMinZoomLevel()) {
            zoom = mapView.getMinZoomLevel();
        }
        if (currentLocationShown) {
            mapView.getController().setCenter(
                    new GeoPoint(LocationsRecorder.getInstance().getLastLocation().lat, LocationsRecorder.getInstance().getLastLocation().lon));
        }
        mapView.getController().setZoom(zoom);

        mapView.getOverlayManager().add(polylineMapObject);
    }

    public void handleError(Throwable t) {

    }

    private void createSpinner() {
        ArrayAdapter<String> statusAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.statuslist));
        statusAdapter.setDropDownViewResource(R.layout.spinner_item_dropdown);
        spinnerStatus.setAdapter(statusAdapter);
        spinnerStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!isPaused && !isInBackground) {
                    switch (position) {
                        case 0:
                            setStatus(Consts.STATUS_FREE);
                            break;
                        case 1:
                            setStatus(Consts.STATUS_PATROL);
                            break;
                        case 2:
                            setStatus(Consts.STATUS_OTHER);
                            break;
                        case 3:
                            setStatus(Consts.STATUS_COMMAND);
                            break;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_LANDSCAPE);
        instance = this;
        prevMessage = null;
        status = Consts.STATUS_FREE;
        Messages.NOTIFY_ID = 0;
        isActionsPopupShown = false;

        Fabric.with(this, new Crashlytics());

        //Configuration.getInstance().load(this, PreferenceManager.getDefaultSharedPreferences(this));
        //Configuration.getInstance().setUserAgentValue(getPackageName());

        setContentView(R.layout.activity_main);

        //mapView = (MapView) findViewById(R.id.mapview);
        //Utils.initMap(mapView, this);
        ButterKnife.bind(this);
        createSpinner();

        showMap();
        Utils.initMap(mapView, this);

        if (getIntent().getStringExtra("action") != null && getIntent().getParcelableExtra("message") != null) {
            if (getIntent().getStringExtra("action").toUpperCase().compareTo("ALARM") == 0) {
                AlarmMessage alarmMessage = getIntent().getParcelableExtra("message");
                ApiClient.getInstance(this).getSosSubject().onNext(alarmMessage);
            } else
            if (getIntent().getStringExtra("action").toUpperCase().compareTo("SOS") == 0) {
                SosMessage sosMessage = getIntent().getParcelableExtra("message");
                ApiClient.getInstance(this).getSosSubject().onNext(sosMessage);
            }
        } else {
            mapView.getController().setCenter(new GeoPoint(55.751574, 37.573856));
            //mapView.getMap().setZoomGesturesEnabled(true);
            getVersionDisposable = ApiClient.getInstance(this).getJournalService().getVersionFromStore()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(versionString -> {
                        versionString = versionString.replace("\"", "");
                        int version = Integer.valueOf(versionString) + Consts.FIRST_STORE_VERSION - 1;
                        if (version > BuildConfig.VERSION_CODE) {
                            startDownload();
                        } else {
                            if (compositeDisposable == null) {
                                compositeDisposable = new CompositeDisposable();
                            }
                            startApp();
                        }
                    }, t -> {
                        Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        new Handler(Looper.getMainLooper()).post(() -> startApp());
                    });
        }
    }

    private void startDownload() {
        final String dirPath = Environment.getExternalStorageDirectory().getPath() + "/" + Consts.downloadDirName + "/";
        File dir = new File(dirPath);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        File file = new File(dir, Consts.downloadFileName);

        final DownloadProgressFragment fragment = new DownloadProgressFragment();
        fragment.prepareDialog(this, R.layout.dialogfragment_download).show();
        DownloadProgressListener listener = new DownloadProgressListener() {
            @Override
            public void update(long bytesRead, long contentLength, boolean done) {
                int percent = Math.round((bytesRead * 100.0f)/contentLength);
                handler.post(() -> ((DownloadProgressFragment) BaseDialogFragment.getInstance()).setProgress(percent));
            }
        };
        final VersionUpdater updater = new VersionUpdater(Consts.JOURNAL_URL, listener);
        updater.downloadAPK(Consts.downloadUrl, file)
                .subscribe(inputStream -> {
                    BaseDialogFragment.getInstance().getDialog().dismiss();
                    if (VersionUpdater.canRunRootCommands()) {
                        updater.doSilentUpdate(MainActivity.this, dirPath + Consts.downloadFileName);
                    } else {
                        updater.doVerboseUpdate(MainActivity.this, dirPath + Consts.downloadFileName);
                    }
                }, t -> {
                    Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    BaseDialogFragment.getInstance().getDialog().dismiss();
                });
    }

    private void startApp() {
        final String dirPath = Environment.getExternalStorageDirectory().getPath() + "/" + Consts.downloadDirName + "/";
        File dir = new File(dirPath);
        if (dir.exists()) {
            dir.delete();
        }

        setButtonBackgrounds();
        setButtonsVisibility();

        subscribeToSocketEvents();

        if (!TextUtils.isEmpty(SharedPreferencesHelper.getUsername(this)) && !TextUtils.isEmpty(SharedPreferencesHelper.getPassword(this))
                && SharedPreferencesHelper.isLoggedIn(this))
        {
            initSocketsAndBackgroundService();
        } else {
            LoginFragment loginFragment = new LoginFragment();
            try {
                loginFragment.showDialog(this);
                if (loginFragment.getDialog() != null) {
                    loginFragment.getDialog().setOnCancelListener((dialog) -> finish());
                }
            } catch (Exception e) {
                Crashlytics.logException(e);
                Toast.makeText(this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
        if (BaseDialogFragment.getInstance() != null) {
            BaseDialogFragment.getInstance().refreshOnConfigChanged(this);
        }
    }

    private void subscribeToSocketEvents() {
        if (dialogDisposable == null || dialogDisposable.isDisposed()) {
            dialogDisposable = ApiClient.getInstance(this).getDialogEventsSubject().observeOn(AndroidSchedulers.mainThread()).subscribe(info -> {
                if (info.layoutId == R.layout.dialogfragment_login) {
                    if (instance == null || isFinishing()) return;
                    if (info.viewId == R.id.btn_ok) {
                        SharedPreferencesHelper.setUsername(this, ((LoginFragment.UserInfo) info.data).name);
                        SharedPreferencesHelper.setPassword(this, ((LoginFragment.UserInfo) info.data).password);
                        SharedPreferencesHelper.setIsLoggedIn(this, true);

                        initSocketsAndBackgroundService();
                    }
                } else if (info.layoutId == R.layout.dialogfragment_sos) {
                    if (instance == null || isFinishing() || LocationsRecorder.getInstance() == null || LocationsRecorder.getInstance().getLastLocation() == null)
                        return;
                    if (info.viewId == R.id.btn_ok) {
                        PlaySound playSound = PlaySound.getInstance(this);
                        if (playSound != null && playSound.isPlaying()) {
                            playSound.stop();
                        }
                        if (status != Consts.STATUS_RIDE) {
                            setStatus(Consts.STATUS_RIDE);

                            if (info.data != null) {
                                clearOSMcache();
                                System.gc();
                                if (System.currentTimeMillis() - routeRequestTime >= LocationsHandler.notify_interval - 1000) {
                                    routeRequestTime = System.currentTimeMillis();
                                    buildRoute((Message) info.data);
                                }

                                new Handler().post(() -> {
                                    socketListener.sendSosOrAlarmApply((Message) info.data);
                                });
                            }
                        }
                    } else if (info.viewId == R.id.btn_cancel) {
                        PlaySound playSound = PlaySound.getInstance(this);
                        if (playSound != null && playSound.isPlaying()) {
                            playSound.stop();
                        }
                        if (prevMessage != (Message) info.data) {
                            prevMessage = (Message) info.data;
                            AlarmDeclineReasonFragment fragment = new AlarmDeclineReasonFragment();
                            fragment.showDialog(this, (Message) info.data);
                        }
                    }
                } else if (info.layoutId == R.layout.dialogfragment_alarm_declined) {
                    if (instance == null || isFinishing()) return;
                    switch (info.viewId) {
                        case R.id.btn_busy: {
                            //if (getStatus() != Consts.STATUS_RIDE && getStatus() != Consts.STATUS_ARRIVED) {
                            //    setStatus(Consts.STATUS_RIDE);
                            //}
                        }
                        ;
                        break;
                        case R.id.btn_broken:
                        case R.id.btn_ok:
                            setStatus(Consts.STATUS_OTHER);
                            break;
                        case R.id.btn_patrol:
                            setStatus(Consts.STATUS_PATROL);
                            break;
                        case R.id.btn_order:
                            setStatus(Consts.STATUS_COMMAND);
                            break;
                    }
                    new Handler().post(() -> {
                        socketListener.sendSosOrAlarmApply((Message) info.data);
                        if (LocationsRecorder.getInstance().getLastLocation() != null) {
                            socketListener.sendLatLon(LocationsRecorder.getInstance().getLastLocation().lat, LocationsRecorder.getInstance().getLastLocation().lon, LocationsRecorder.getInstance().getLastLocation().accuracy);
                        }
                    });
                } else if (info.layoutId == R.layout.dialogfragment_report) {
                    if (instance == null || isFinishing()) return;
                    switch (info.viewId) {
                        case R.id.btn_normal:
                            new Handler().post(() -> {
                                socketListener.sendEndStatus(Consts.RESULT_NORMAL);
                            });
                            break;
                        case R.id.btn_view_problems:
                            new Handler().post(() -> {
                                socketListener.sendEndStatus(Consts.RESULT_HARD_TO_VIEW);
                            });
                            break;
                        case R.id.btn_penetration:
                            new Handler().post(() -> {
                                socketListener.sendEndStatus(Consts.RESULT_PENETRATION);
                            });
                            break;
                        case R.id.btn_perimeter_fault:
                            new Handler().post(() -> {
                                socketListener.sendEndStatus(Consts.RESULT_PERIMETER_FAULT);
                            });
                            break;
                        case R.id.btn_gone:
                            new Handler().post(() -> {
                                if (ApiClient.getInstance(this).getLastAlarmOrSos() instanceof SosMessage) {
                                    socketListener.sendIsFree(((SosMessage) ApiClient.getInstance(this).getLastAlarmOrSos()).id);
                                } else if (ApiClient.getInstance(this).getLastAlarmOrSos() instanceof AlarmMessage) {
                                    socketListener.sendIsFree(((AlarmMessage) ApiClient.getInstance(this).getLastAlarmOrSos()).id);
                                }
                            });
                            ConfirmLeaveFragment fragment = new ConfirmLeaveFragment();
                            fragment.showDialog(this);
                            fragment.getDialog().setCanceledOnTouchOutside(false);
                            fragment.getDialog().setCancelable(false);
                            break;
                    }
                } else if (info.layoutId == R.layout.dialogfragment_confirm_leave) {
                    if (info.viewId == R.id.btn_ok) {
                        Message sos = ApiClient.getInstance(this).getLastIsFree();
                        if (sos != null) {
                            if (((IsFreeMessage) sos).message == 1) {
                                setStatus(Consts.STATUS_FREE);
                                //ApiClient.getInstance(this).setLastAlarmOrSos(null);
                            } else {
                                setStatus(Consts.STATUS_ARRIVED);
                                Toast.makeText(this, getResources().getString(R.string.str_leave_not_confirmed), Toast.LENGTH_SHORT).show();
                                ReportFragment fragment = new ReportFragment();
                                fragment.showDialog(this);
                                fragment.getDialog().setCanceledOnTouchOutside(false);
                                fragment.getDialog().setCancelable(false);
                            }
                        }
                        ApiClient.getInstance(this).setLastIsFree(null);
                    }
                } else {
                    if (info.viewId == R.id.btn_ok) {

                    }
                }
            });
        }

        if (sosDisposable == null || sosDisposable.isDisposed()) {
            sosDisposable = ApiClient.getInstance(this).getSosSubject().observeOn(AndroidSchedulers.mainThread()).subscribe(sos -> {
                if (sos == prevMessage) return;
                if (sos instanceof IsFreeMessage) {
                    ApiClient.getInstance(this).setLastIsFree(sos);
                    if (BaseDialogFragment.getInstance() != null && BaseDialogFragment.getInstance() instanceof ConfirmLeaveFragment) {
                        ((ConfirmLeaveFragment) BaseDialogFragment.getInstance()).enableOK();
                    }
                    return;
                }

                if (sos instanceof DeclineAlarmMessage) {
                    setStatus(Consts.STATUS_FREE);
                    return;
                }

                synchronized (this) {
                    if (PlaySound.getInstance(this).isPlaying()) {
                        new Handler().postDelayed(() -> {
                            PlaySound.getInstance(this).stop();
                            PlaySound.getInstance(this).play(SharedPreferencesHelper.getSoundFilePath(this));
                        }, 2000);
                    } else {
                        PlaySound.getInstance(this).play(SharedPreferencesHelper.getSoundFilePath(this));
                    }
                    if (isPaused) {
                        if (isInBackground) {
                            Messages.sendNotification(sos, this);
                            prevMessage = sos;
                            return;
                        }
                    }
                }

                prevMessage = sos;
                AcceptSosFragment acceptSosFragment = new AcceptSosFragment();
                acceptSosFragment.showDialog(this, sos);
                if (sos instanceof SosMessage) {
                    tvAddress.setText(((SosMessage) sos).address);
                    tvObjectName.setText("");
                    if (acceptSosFragment.getDialog() != null) {
                        acceptSosFragment.getDialog().setCanceledOnTouchOutside(false);
                        acceptSosFragment.getDialog().setCancelable(false);
                        acceptSosFragment.getDialog().setOnCancelListener((dialog -> {
                            PlaySound playSound = PlaySound.getInstance(this);
                            if (playSound != null && playSound.isPlaying()) {
                                playSound.stop();
                            }
                            SosApplyMessage msg = new SosApplyMessage();
                            msg.action = "sosApply";
                            msg.result = false;
                            msg.description = "Причина не указана";
                            msg.id = ((SosMessage) sos).id;
                            dialog.dismiss();
                            socketListener.sendSosOrAlarmApply(msg);
                        }));
                    }
                } else if (sos instanceof AlarmMessage) {
                    String addr = ((AlarmMessage) sos).getText();
                    tvAddress.setText(addr);
                    tvObjectName.setText(((AlarmMessage) sos).name);
                    if (acceptSosFragment.getDialog() != null) {
                        acceptSosFragment.getDialog().setCanceledOnTouchOutside(false);
                        acceptSosFragment.getDialog().setCancelable(false);
                        acceptSosFragment.getDialog().setOnCancelListener((dialog -> {
                            PlaySound playSound = PlaySound.getInstance(this);
                            if (playSound != null && playSound.isPlaying()) {
                                playSound.stop();
                            }
                            AlarmApplyMessage msg = new AlarmApplyMessage();
                            msg.action = "alarmApply";
                            msg.result = false;
                            msg.description = "Причина не указана";
                            msg.id = ((AlarmMessage) sos).id;
                            dialog.dismiss();
                            socketListener.sendSosOrAlarmApply(msg);
                        }));
                    }
                }
            });
        }

        bgColorDisposable = ApiClient.getInstance(this).getBgColorSubject().observeOn(AndroidSchedulers.mainThread()).subscribe(color -> changeButtonColors(color));
        sosBgColorDisposable = ApiClient.getInstance(this).getSosBgColorSubject().observeOn(AndroidSchedulers.mainThread()).subscribe(color -> changeSosButtonColor(color));
        routeColorDisposable = ApiClient.getInstance(this).getRouteColorSubject().observeOn(AndroidSchedulers.mainThread()).subscribe(color -> changeRouteColor(color));
        if (compositeDisposable == null) {
            compositeDisposable = new CompositeDisposable();
        }
        compositeDisposable.add(bgColorDisposable);
        compositeDisposable.add(sosBgColorDisposable);
        compositeDisposable.add(routeColorDisposable);
    }

    private void buildRoute(Message msg) {
        double lat=0, lon=0;
        if (msg instanceof SosApplyMessage) {
            ApiClient.getInstance(this).setLastAlarmOrSos((Message) msg);
            //lat = 58.563876;
            //lon = 31.28674;
            lat = ((SosApplyMessage) msg).lat;
            lon = ((SosApplyMessage) msg).lon;
        } else
        if (msg instanceof AlarmApplyMessage && prevMessage != null && prevMessage instanceof AlarmMessage) {
            ApiClient.getInstance(this).setLastAlarmOrSos(prevMessage);
            lat = ((AlarmMessage) prevMessage).lat;
            lon = ((AlarmMessage) prevMessage).lon;
        } else
        if (msg instanceof AlarmMessage) {
            lat = ((AlarmMessage) msg).lat;
            lon = ((AlarmMessage) msg).lon;
        }
        routeRequest(LocationsRecorder.getInstance().getLastLocation().lat, LocationsRecorder.getInstance().getLastLocation().lon,
                lat, lon);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        if (getIntent().getStringExtra("action") != null && getIntent().getParcelableExtra("message") != null) {
            if (getIntent().getStringExtra("action").toUpperCase().compareTo("ALARM") == 0) {
                AlarmMessage alarmMessage = getIntent().getParcelableExtra("message");
                ApiClient.getInstance(this).getSosSubject().onNext(alarmMessage);
            } else
            if (getIntent().getStringExtra("action").toUpperCase().compareTo("SOS") == 0) {
                SosMessage sosMessage = getIntent().getParcelableExtra("message");
                ApiClient.getInstance(this).getSosSubject().onNext(sosMessage);
            }
        } else {
            if (PlaySound.getInstance(this).isPlaying()) {
                PlaySound.getInstance(this).stop();
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (isActionsPopupShown) {
            popupWindow.dismiss();
        } else {
            super.onBackPressed();
        }
    }

    private void initSocketsAndBackgroundService() {
        SocketListener listenerLocal = socketListener;
        if (listenerLocal == null) {
            synchronized (SocketListener.class) {
                listenerLocal = new SocketListener();
                listenerLocal.init(this);
                socketListener = listenerLocal;
            }
        }

        synchronized (this) {
            if (isLocationPermissionGranted()) {
                ApiClient.getInstance(this).initLocationsApi(this);
                GoogleApiClient googleApiClient = ApiClient.getInstance(this).getGoogleApiClient();
                if (googleApiClient != null && !googleApiClient.isConnected()) {
                    googleApiClient.connect();
                }
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onStart() {
        super.onStart();
    }


    @Override
    public void onPause() {
        super.onPause();
        clearOSMcache();
        System.gc();
        //mapView.setVisibility(View.GONE);
        isPaused = true;
    }

    @Override
    public void onResume() {
        super.onResume();
        //mapView.setVisibility(View.VISIBLE);
        isPaused = false;
        setInBackground(false);
    }

    @Override
    public void onDestroy() {
        instance = null;
        if (isFinishing()) {
            doOnDestroy();
        }
        super.onDestroy();
    }

    private void doOnDestroy() {
        //SharedPreferencesHelper.setUsername(this, "");
        //SharedPreferencesHelper.setPassword(this, "");
        GoogleApiClient googleApiClient = ApiClient.getInstance(this).getGoogleApiClient();
        if (googleApiClient != null && googleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, ApiClient.getInstance(this).getLocationsHandler());
            googleApiClient.disconnect();
        }
        clearOSMcache();
        LocationsRecorder.clear();
        PlaySound.clear();
        ApiClient.clear();
        if (dialogDisposable != null) {
            dialogDisposable.dispose();
            dialogDisposable = null;
        }
        if (sosDisposable != null) {
            sosDisposable.dispose();
            sosDisposable = null;
        }
        if (compositeDisposable != null) {
            compositeDisposable.dispose();
            compositeDisposable = null;
        }
        if (socketListener != null) {
            socketListener.disconnect();
            socketListener = null;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Consts.REQUEST_CODE_PERMISSION_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    ApiClient.getInstance(this).initLocationsApi(this);
                    GoogleApiClient googleApiClient = ApiClient.getInstance(this).getGoogleApiClient();
                    if (googleApiClient != null && !googleApiClient.isConnected()) {
                        googleApiClient.connect();
                    }
                }
                break;
        }
    }

    public boolean isLocationPermissionGranted() {
        if (ContextCompat.checkSelfPermission(this, Consts.PERMISSION_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Consts.PERMISSION_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Consts.PERMISSION_WIFI_STATE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Consts.PERMISSION_WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Consts.PERMISSION_NETWORK_STATE) == PackageManager.PERMISSION_GRANTED)
        {
            return true;
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Consts.PERMISSION_FINE_LOCATION) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(this, Consts.PERMISSION_COARSE_LOCATION) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(this, Consts.PERMISSION_WIFI_STATE) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(this, Consts.PERMISSION_WRITE_EXTERNAL_STORAGE) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(this, Consts.PERMISSION_NETWORK_STATE))
            {
                new android.support.v7.app.AlertDialog.Builder(this)
                        .setMessage(R.string.read_gps_message)
                        .setCancelable(false)
                        .setPositiveButton(R.string.str_ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Consts.PERMISSION_FINE_LOCATION, Consts.PERMISSION_COARSE_LOCATION, Consts.PERMISSION_NETWORK_STATE,
                                                        Consts.PERMISSION_WIFI_STATE, Consts.PERMISSION_WRITE_EXTERNAL_STORAGE},
                                                Consts.REQUEST_CODE_PERMISSION_LOCATION);
                                    }
                                }
                        ).show();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Consts.PERMISSION_FINE_LOCATION, Consts.PERMISSION_COARSE_LOCATION, Consts.PERMISSION_NETWORK_STATE,
                                Consts.PERMISSION_WIFI_STATE, Consts.PERMISSION_WRITE_EXTERNAL_STORAGE},
                        Consts.REQUEST_CODE_PERMISSION_LOCATION);
            }
        }
        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Consts.AUDIO_REQUEST && null != data) {
            Uri uri = data.getData();
            try {
                String path = PlaySound.getInstance(this).getAudioPath(uri);
                SharedPreferencesHelper.setSoundFilePath(this, path);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void goToLocation() {
        if (LocationsRecorder.getInstance() != null && LocationsRecorder.getInstance().getLastLocation() != null) {
            mapView.getController().setCenter(
                    new GeoPoint(LocationsRecorder.getInstance().getLastLocation().lat, LocationsRecorder.getInstance().getLastLocation().lon));
            mapView.getController().setZoom(17);
        }
        //mapView.getController().setZoom(17.0);
    }

    private void setButtonBackgrounds() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            btnStatus.setBackgroundResource(R.drawable.btn_v21_no_corners);
            //btnStatus.setStateListAnimator(null);
            btnLastObject.setBackgroundResource(R.drawable.btn_v21_no_corners);
            //btnLastObject.setStateListAnimator(null);
            btnLog.setBackgroundResource(R.drawable.btn_v21_no_corners);
            //btnLog.setStateListAnimator(null);
            btnSOS.setBackgroundResource(R.drawable.btn_v21_red_no_corners);
            //btnSOS.setStateListAnimator(null);
            btnSettings.setBackgroundResource(R.drawable.btn_v21_no_corners);
            //btnSettings.setStateListAnimator(null);
            btnArrived.setBackgroundResource(R.drawable.btn_v21_no_corners);
            //btnArrived.setStateListAnimator(null);
            btnHardToMove.setBackgroundResource(R.drawable.btn_v21_no_corners);
            //btnHardToMove.setStateListAnimator(null);
        } else {
            btnStatus.setBackgroundResource(R.drawable.btn_rounded_no_corners);
            btnLastObject.setBackgroundResource(R.drawable.btn_rounded_no_corners);
            btnLog.setBackgroundResource(R.drawable.btn_rounded_no_corners);
            btnSOS.setBackgroundResource(R.drawable.btn_rounded_red_no_corners);
            btnSettings.setBackgroundResource(R.drawable.btn_rounded_no_corners);
            btnArrived.setBackgroundResource(R.drawable.btn_rounded_no_corners);
            btnHardToMove.setBackgroundResource(R.drawable.btn_rounded_no_corners);
        }

        //Toast.makeText(this, Integer.toHexString(color), Toast.LENGTH_SHORT).show();
        changeButtonColors(SharedPreferencesHelper.getButtonsBackgroundColor(this));
        changeSosButtonColor(SharedPreferencesHelper.getSOSButtonBackgroundColor(this));
    }

    public void changeButtonColors(int color) {
        Utils.setButtonColors(btnStatus, color, Utils.getHighlightedColor(color));
        Utils.setButtonColors(btnLog, color, Utils.getHighlightedColor(color));
        Utils.setButtonColors(btnLastObject, color, Utils.getHighlightedColor(color));
        Utils.setButtonColors(btnSettings, color, Utils.getHighlightedColor(color));
        Utils.setButtonColors(btnArrived, color, Utils.getHighlightedColor(color));
        Utils.setButtonColors(btnHardToMove, color, Utils.getHighlightedColor(color));
    }

    public void changeSosButtonColor(int color) {
        Utils.setButtonColors(btnSOS, color, Utils.getHighlightedColor(color));
    }

    public void changeRouteColor(int color) {
        if (polylineMapObject != null) {
            polylineMapObject.setColor(color);
        }
    }

    public void setButtonsVisibility() {
        synchronized (this) {
            switch (status) {
                case Consts.STATUS_RIDE_DIFFICULT:
                case Consts.STATUS_RIDE: {
                    llAcceptedAlarmButtons.setVisibility(View.VISIBLE);
                    llSpinner.setVisibility(View.GONE);
                    btnArrived.setVisibility(View.VISIBLE);
                    space1.setVisibility(View.GONE);
                    btnFree.setVisibility(View.GONE);
                    space2.setVisibility(View.GONE);
                    btnHardToMove.setVisibility(View.VISIBLE);
                };
                break;
                case Consts.STATUS_PATROL:
                case Consts.STATUS_COMMAND:
                case Consts.STATUS_OTHER: {
                    llAcceptedAlarmButtons.setVisibility(View.VISIBLE);
                    llSpinner.setVisibility(View.VISIBLE);
                    switch (status) {
                        case Consts.STATUS_PATROL: spinnerStatus.setSelection(1); break;
                        case Consts.STATUS_COMMAND: spinnerStatus.setSelection(3); break;
                        case Consts.STATUS_OTHER: spinnerStatus.setSelection(2); break;
                    }
                    spinnerStatus.setEnabled(false);
                    btnArrived.setVisibility(View.GONE);
                    space1.setVisibility(View.VISIBLE);
                    btnFree.setVisibility(View.VISIBLE);
                    space2.setVisibility(View.VISIBLE);
                    btnHardToMove.setVisibility(View.GONE);
                };
                break;
                case Consts.STATUS_ARRIVED:
                    llSpinner.setVisibility(View.GONE);
                    llAcceptedAlarmButtons.setVisibility(View.GONE);
                    break;
                case Consts.STATUS_FREE:
                    llSpinner.setVisibility(View.VISIBLE);
                    spinnerStatus.setEnabled(true);
                    spinnerStatus.setSelection(0);
                    llAcceptedAlarmButtons.setVisibility(View.GONE);
                    break;
            }

            btnLocation.setVisibility(currentLocationShown ? View.GONE : View.VISIBLE);
        }
    }

    private void showJournal(List<JournalRecord> list) {
        wrapper.setVisibility(View.GONE);
        rvJournal.setVisibility(View.VISIBLE);
        mapView.setVisibility(View.GONE);
        llButtons.setVisibility(View.GONE);
        btnLocation.setVisibility(View.GONE);

        JournalAdapter adapter = new JournalAdapter();
        rvJournal.setAdapter(adapter);
        rvJournal.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvJournal.addItemDecoration(new SimpleDividerItemDecoration(this));
        adapter.setData(list);
    }

    private void showMap() {
        wrapper.setVisibility(View.VISIBLE);
        rvJournal.setVisibility(View.GONE);
        mapView.setVisibility(View.VISIBLE);
        btnLastObject.setEnabled(ApiClient.getInstance(this).getLastAlarmOrSos() != null);
        llButtons.setVisibility(View.VISIBLE);
        setButtonsVisibility();
    }

    public void showActionsPopup(List<ActionRecord> actionsList, View button) {
        WindowManager wm = ( WindowManager ) getSystemService( Context.WINDOW_SERVICE );
        Display display = wm.getDefaultDisplay();

        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics( metrics );
        int screenWidth = metrics.widthPixels;
        int screenHeight = metrics.heightPixels;

        final View popupView = LayoutInflater.from(this).inflate(R.layout.actions_popup, null);
        popupWindow = new PopupWindow(popupView, WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        popupWindow.setWidth((screenWidth*2)/3);
        popupWindow.setHeight(screenHeight/2);

        RecyclerView recyclerView = (RecyclerView) popupView.findViewById(R.id.rv_actions);
        ActionsAdapter adapter = new ActionsAdapter();
        adapter.setData(actionsList);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        popupWindow.setOnDismissListener(() -> {
            isActionsPopupShown = false;
            popupWindow = null;
        });
        isActionsPopupShown = true;
        popupWindow.showAsDropDown(button);
    }

    private void dismissPopup() {
        if (isActionsPopupShown) {
            popupWindow.dismiss();
        }
    }

    @OnClick(R.id.btn_log)
    void LogClick() {
        if (compositeDisposable == null) return;
        logDisposable = ApiClient.getInstance(this).getJournalService().getJournal(SharedPreferencesHelper.getUsername(this), SharedPreferencesHelper.getPassword(this))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(journal -> showJournal(journal), t -> {});
        compositeDisposable.add(logDisposable);
    }

    @OnClick(R.id.btn_sos)
    void SOSClick() {
        dismissPopup();
        showMap();
        if (LocationsRecorder.getInstance().getLastLocation() != null && socketListener != null) {
            socketListener.sendSos(LocationsRecorder.getInstance().getLastLocation().lat, LocationsRecorder.getInstance().getLastLocation().lon, false);
            new Handler().postDelayed(() -> showConfirmSosDialog(), 200);
            //new Handler().postDelayed(() -> socketListener.sendDebugSosOrAlarm(LocationsRecorder.getInstance().getLastLocation().lat, LocationsRecorder.getInstance().getLastLocation().lon, "sos"), 3000);
        }
    }

    private void showConfirmSosDialog() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.confirm_sos_txt)
                .setCancelable(false)
                .setPositiveButton(R.string.btn_yes, (dialog, which) -> {
                    dialog.dismiss();
                    socketListener.sendSos(LocationsRecorder.getInstance().getLastLocation().lat, LocationsRecorder.getInstance().getLastLocation().lon, true);
                })
                .setNegativeButton(R.string.btn_no, (dialog, which) -> {
                    dialog.dismiss();
                })
                .show();
    }

    private void showConfirmLogoutDialog() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.confirm_exit_txt)
                .setCancelable(false)
                .setPositiveButton(R.string.btn_yes, (dialog, which) -> {
                    dialog.dismiss();
                    doLogout();
                })
                .setNegativeButton(R.string.btn_no, (dialog, which) -> {
                    dialog.dismiss();
                })
                .show();
    }

    @OnClick(R.id.btn_status)
    void statusClick() {
        dismissPopup();
        showMap();
    }

    @OnClick(R.id.btn_last_object)
    void LastObjectClick() {
        dismissPopup();
        if (ApiClient.getInstance(this).getLastAlarmOrSos() != null) {
            showMap();
            buildRoute(ApiClient.getInstance(this).getLastAlarmOrSos());
            setStatus(Consts.STATUS_RIDE);
        }
    }

    @OnClick(R.id.btn_settings)
    void SettingsClick() {
        dismissPopup();
        showMap();
        SettingsFragment fragment = new SettingsFragment();
        fragment.showDialog(this);
    }

    private void doLogout() {
        dismissPopup();
        //SharedPreferencesHelper.setUsername(this, "");
        //SharedPreferencesHelper.setPassword(this, "");
        SharedPreferencesHelper.setIsLoggedIn(this, false);
        finish();
    }

    @OnClick(R.id.btn_logout)
    void LogoutClick() {
        showConfirmLogoutDialog();
    }

    @OnClick(R.id.btn_free)
    void FreeClick() {
        if (!isPaused && !isInBackground) {
            setStatus(Consts.STATUS_FREE);
        }
    }

    @OnClick(R.id.location_btn)
    void locationClick() {
        goToLocation();
        btnLocation.setVisibility(View.GONE);
        currentLocationShown = true;
    }

    @OnClick(R.id.btn_arrived)
    void arrivedClick() {
        setStatus(Consts.STATUS_ARRIVED);
        ReportFragment fragment = new ReportFragment();
        fragment.showDialog(this);
        fragment.getDialog().setCanceledOnTouchOutside(false);
        fragment.getDialog().setCancelable(false);
        if (socketListener != null) {
            if (ApiClient.getInstance(this).getLastAlarmOrSos() instanceof SosMessage) {
                socketListener.sendInfo(((SosMessage) ApiClient.getInstance(this).getLastAlarmOrSos()).id, getResources().getString(R.string.btn_arrived));
            } else if (ApiClient.getInstance(this).getLastAlarmOrSos() instanceof AlarmMessage) {
                socketListener.sendInfo(((AlarmMessage) ApiClient.getInstance(this).getLastAlarmOrSos()).id, getResources().getString(R.string.btn_arrived));
            }
        }
    }

    @OnClick(R.id.btn_hard_to_move)
    void hardToMoveClick() {
        if (compositeDisposable == null) return;
        setStatus(Consts.STATUS_RIDE_DIFFICULT);
        if (socketListener != null) {
            socketListener.sendLatLon(LocationsRecorder.getInstance().getLastLocation().lat, LocationsRecorder.getInstance().getLastLocation().lon, LocationsRecorder.getInstance().getLastLocation().accuracy);
        } else return;
        if (ApiClient.getInstance(this).getLastAlarmOrSos() instanceof SosMessage) {
            socketListener.sendInfo(((SosMessage) ApiClient.getInstance(this).getLastAlarmOrSos()).id, getResources().getString(R.string.btn_hard_to_move));
        } else
        if (ApiClient.getInstance(this).getLastAlarmOrSos() instanceof AlarmMessage) {
            socketListener.sendInfo(((AlarmMessage) ApiClient.getInstance(this).getLastAlarmOrSos()).id, getResources().getString(R.string.btn_hard_to_move));
        }
        if (timeDisposable != null) {
            compositeDisposable.remove(timeDisposable);
        }
        rideDifficultLockRemainingTime = Consts.RIDE_DIFFICULT_LOCK_TIME;
        btnHardToMove.setEnabled(false);
        timeDisposable = Flowable.interval(0, 1000, TimeUnit.MILLISECONDS)
                .take(Consts.RIDE_DIFFICULT_LOCK_TIME)
                .map(step -> Consts.RIDE_DIFFICULT_LOCK_TIME - step)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(sec -> showSeconds(sec), t -> endTimer(), () -> endTimer());
    }

    private void showSeconds(long sec) {
        String txt = getResources().getString(R.string.remains) + " " + sec + " ";
        String[] seconds = getResources().getStringArray(R.array.second_forms);
        int forms[] = {2, 0, 1, 1, 1};
        int form = 2;
        if ((sec < 10 || sec > 19) && (sec % 10 < 5)) {
            form = forms[(int) sec % 10];
        }
        txt = txt + seconds[form];
        btnHardToMove.setText(txt);
    }

    private void endTimer() {
        btnHardToMove.setText(R.string.btn_hard_to_move);
        btnHardToMove.setEnabled(true);
    }

    @Override
    public void onUpdateMapAfterUserInteraction() {
        currentLocationShown = false;
        btnLocation.setVisibility(View.VISIBLE);
        PlaySound playSound = PlaySound.getInstance(this);
        //if (playSound != null && playSound.isPlaying()) {
        //    playSound.stop();
        //}
    }

    public void clearOSMcache() {
        mapView.getTileProvider().clearTileCache();
    }

    @Override
    public void onGoogleApiConnected(boolean result) {
        //if (isLocationPermissionGranted() && ApiClient.getInstance(this).getGoogleApiClient() != null) {
        if (isLocationPermissionGranted()) {
            @SuppressLint("MissingPermission") Location location = LocationServices.FusedLocationApi.getLastLocation(ApiClient.getInstance(this).getGoogleApiClient());
            ApiClient.getInstance(instance).setLatitude(location.getLatitude());
            ApiClient.getInstance(instance).setLongitude(location.getLongitude());
            ApiClient.getInstance(instance).startLocationUpdates();
        }
    }

    @Override
    public void handleLocationChanged(Location location) {
        float accuracy = location.getAccuracy();
        if (LocationsRecorder.getInstance().getLastLocation() != null) {
            ApiClient.getInstance(instance).setLatitude(LocationsRecorder.getInstance().getLastLocation().lat);
            ApiClient.getInstance(instance).setLongitude(LocationsRecorder.getInstance().getLastLocation().lon);
            if (socketListener != null) {
                socketListener.sendLatLon(LocationsRecorder.getInstance().getLastLocation().lat, LocationsRecorder.getInstance().getLastLocation().lon, accuracy);
            }
            if (!isPaused) {
                if (currentLocationShown) {
                    goToLocation();
                }
                if (LocationsRecorder.getInstance().needUpdate()) {
                    CityGuideRouteResponse.GeometryFeature geometry = LocationsRecorder.getInstance().getGeometry();
                    routeRequest(LocationsRecorder.getInstance().getLastLocation().lat, LocationsRecorder.getInstance().getLastLocation().lon,
                            geometry.coordinates.get(geometry.coordinates.size() - 1).get(1), geometry.coordinates.get(geometry.coordinates.size() - 1).get(0));
                } else {
                    if (LocationsRecorder.getInstance().getGeometry() != null &&
                            (LocationsRecorder.getInstance().getLastLocation().lat != ApiClient.getInstance(instance).getLatitude() ||
                                    LocationsRecorder.getInstance().getLastLocation().lon != ApiClient.getInstance(instance).getLongitude())) {
                        if (polylineMapObject != null) {
                            mapView.getOverlays().remove(polylineMapObject);
                            polylineMapObject = null;
                            System.gc();
                        }

                        Pair<List<GeoPoint>, LocationsRecorder.MapRect> result = LocationsRecorder.getInstance().getCurrentPolyline();
                        if (result != null) {
                            addPolyline(result.first, result.second);
                        }
                    }
                }
            }
        }
    }
}
