package ru.ohrana_neva.client.data;

import android.location.Location;
import android.support.v4.util.Pair;

import org.osmdroid.util.GeoPoint;

import java.util.ArrayList;
import java.util.List;

import ru.ohrana_neva.client.Consts;
import ru.ohrana_neva.client.data.model.CityGuideRouteResponse;

public class LocationsRecorder {
    private static volatile LocationsRecorder recorder = null;
    private ArrayList<PointLocal> locations;
    private int currentMinIndex = 0;
    private CityGuideRouteResponse.GeometryFeature geometry;
    private int pointsOnRoute = 0;
    private Object lock;

    // fake location - will be removed later
    //private int fakeLeg;
    //private double fakeMul;
    private Location lastLocation;

    public static LocationsRecorder getInstance() {
        LocationsRecorder localRef = recorder;
        if (localRef == null) {
            synchronized (LocationsRecorder.class) {
                localRef = new LocationsRecorder();
                localRef.locations = new ArrayList<>();
                localRef.lock = new Object();

                //localRef.fakeLeg = 0;
                //localRef.fakeMul = 0;

                recorder = localRef;
            }
        }
        return localRef;
    }

    public static void clear() {
        recorder = null;
    }

    public void addLocation(Location location) {
        synchronized (lock) {
            lastLocation = location;

            if (locations.size() == Consts.LOCATIONS_BUF_SIZE) {
                locations.remove(0);
            }
            PointLocal p = new PointLocal();
            //if (geometry != null && geometry.coordinates != null && geometry.coordinates.size() > 0 && locations.size() > 0) {
            //    p = getFakeLocation();
            //} else {
                p.lat = location.getLatitude();
                p.lon = location.getLongitude();
                p.accuracy = location.getAccuracy();
                p.turnedFromRoute = false;
            //}

            if (geometry != null && geometry.coordinates != null && geometry.coordinates.size() > 0) {
                Pair<Double, Boolean> dist = calcDistanceToSection(p.lat, p.lon, geometry.coordinates.get(currentMinIndex).get(0), geometry.coordinates.get(currentMinIndex).get(1),
                        geometry.coordinates.get(currentMinIndex+1).get(0), geometry.coordinates.get(currentMinIndex+1).get(1));
                Pair<Double, Boolean> dist2 = new Pair<>((double) p.accuracy*2, false);
                if (dist.first > p.accuracy || !dist.second) {
                    int index = currentMinIndex;
                    for (int i = currentMinIndex; i < geometry.coordinates.size() - 2; i++) {
                        dist2 = calcDistanceToSection(p.lat, p.lon, geometry.coordinates.get(i).get(1), geometry.coordinates.get(i).get(0),
                                geometry.coordinates.get(i+1).get(1), geometry.coordinates.get(i+1).get(0));
                        if (dist2.first <= dist.first || (dist2.first == 0 && dist2.second)) {
                            dist = dist2;
                            index = i;
                        }
                    }
                    currentMinIndex = index;
                    p.turnedFromRoute = (dist2.first > p.accuracy);
                }
            }

            if (locations.size() == 0) {
                pointsOnRoute++;
                locations.add(p);
            } else
            if (locations.get(locations.size() - 1).lat != p.lat || locations.get(locations.size() - 1).lon != p.lon) {
                pointsOnRoute++;
                locations.add(p);
            }

            //if (pointsOnRoute >= Consts.LOCATIONS_TO_UPDATE) {
            //    if (geometry != null && geometry.coordinates != null && geometry.coordinates.size() > 0) {
            //        fakeLeg = (2 * fakeLeg + geometry.coordinates.size() - 1) / 3;
            //        fakeMul = Math.random();
            //    }
            //}
        }
    }

    public void addLocationDebug() {
        addLocation(lastLocation);
    }

    public PointLocal getLastLocation() {
        synchronized (lock) {
            if (locations != null && locations.size() > 0) {
                return locations.get(locations.size() - 1);
            } else {
                return null;
            }
        }
    }
/*
    public PointLocal getFakeLocation() {
        if (geometry != null && geometry.coordinates != null && geometry.coordinates.size() > 0 && locations.size() > 0) {
            PointLocal p = new PointLocal();
            if (fakeLeg >= geometry.coordinates.size() - 1) {
                fakeLeg = 0;
            }
            if (geometry.coordinates.size() > 1) {
                p.lat = geometry.coordinates.get(fakeLeg).get(1) * fakeMul + geometry.coordinates.get(fakeLeg + 1).get(1) * (1 - fakeMul);
                p.lon = geometry.coordinates.get(fakeLeg).get(0) * fakeMul + geometry.coordinates.get(fakeLeg + 1).get(0) * (1 - fakeMul);
            } else {
                p.lat = geometry.coordinates.get(0).get(1);
                p.lon = geometry.coordinates.get(0).get(0);
            }
            System.out.println("fake lat: " + p.lat + ", lon: " + p.lon);
            p.accuracy = locations.get(0).accuracy;
            p.turnedFromRoute = false;
            return p;
        } else return null;
    }
*/
    public void setGeometry(CityGuideRouteResponse.GeometryFeature geometry) {
        synchronized (lock) {
            if (this.geometry != null) {
                this.geometry.coordinates.clear();
            } else {
                this.geometry = new CityGuideRouteResponse.GeometryFeature();
                this.geometry.coordinates = new ArrayList<>();
            }
            if (geometry != null) {
                this.geometry.coordinates.addAll(geometry.coordinates);
            }
            this.currentMinIndex = 0;
            this.pointsOnRoute = 0;
            for (PointLocal point : this.locations) {
                point.turnedFromRoute = false;
            }
        }
    }

    public CityGuideRouteResponse.GeometryFeature getGeometry() {
        return geometry;
    }

    public boolean needUpdate() {
        synchronized (lock) {
            if (geometry != null && geometry.coordinates != null && geometry.coordinates.size() > 0) {
                if (pointsOnRoute >= Consts.LOCATIONS_TO_UPDATE) return true;
                int cnt = 0;
                for (int i = 0; i < locations.size(); i++) {
                    if (locations.get(i).turnedFromRoute) cnt++;
                }
                if (cnt >= Consts.LOCATIONS_TO_TURN) return true;
            }
            return false;
        }
    }

    public double CalculationByDistance(double lat1, double lon1, double lat2, double lon2) {
        int Radius = 6371;// radius of earth in Km
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1)) *
                Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
        double c = 2 * Math.asin(Math.sqrt(a));

        return Radius * c;
    }

    public Pair<Double, Boolean> calcDistanceToSection(double lat, double lon, double lat1, double lon1, double lat2, double lon2) {
        double dlat = lat2 - lat1;
        double dlon = lon2 - lon1;
        double dlat1 = lat1 - lat;
        double dlon1 = lon1 - lon;
        double dlat2 = lat2 - lat;
        double dlon2 = lon2 - lon;
        if (dlat1 == 0 && dlon1 == 0) return new Pair<>(0d, true);
        if (dlat2 == 0 && dlon2 == 0) return new Pair<>(0d, false);
        double dist1inMeters = CalculationByDistance(lat, lon, lat1, lon1);
        if (dlat2*dlat2 + dlon2*dlon2 + dlat*dlat + dlon*dlon - dlat1*dlat1 - dlon1*dlon1 < 0) {
            return new Pair<>(Math.sqrt(dlat2*dlat2 + dlon2*dlon2)*(dist1inMeters/Math.sqrt(dlat1*dlat1 + dlon1*dlon1)), false);
        } else
        if (dlat1*dlat1 + dlon1*dlon1 + dlat*dlat + dlon*dlon - dlat2*dlat2 - dlon2*dlon2 < 0) {
            return new Pair<>(dist1inMeters, false);
        } else {
            double d = (Math.abs(dlat2*dlon1 - dlat1*dlon2)/Math.sqrt(dlat*dlat + dlon*dlon))*(dist1inMeters/Math.sqrt(dlat1*dlat1 + dlon1*dlon1));
            return new Pair<>(d, true);
        }
    }

    public Pair<List<GeoPoint>, MapRect> getCurrentPolyline() {
        if (geometry != null && geometry.coordinates != null && geometry.coordinates.size() > 0 && getLastLocation() != null) {
            ArrayList<GeoPoint> points = new ArrayList<>();
            double minLat = Math.round(getLastLocation().lat);
            double minLon = Math.round(getLastLocation().lon);
            double maxLat = Math.round(getLastLocation().lat);
            double maxLon = Math.round(getLastLocation().lon);
            points.add(new GeoPoint(getLastLocation().lat, getLastLocation().lon));
            for (int i = currentMinIndex + 1; i < geometry.coordinates.size(); i++) {
                points.add(new GeoPoint(geometry.coordinates.get(i).get(1), geometry.coordinates.get(i).get(0)));
                if (geometry.coordinates.get(i).get(0) < minLat) minLat = geometry.coordinates.get(i).get(1);
                if (geometry.coordinates.get(i).get(1) < minLon) minLon = geometry.coordinates.get(i).get(0);
                if (geometry.coordinates.get(i).get(0) > maxLat) maxLat = geometry.coordinates.get(i).get(1);
                if (geometry.coordinates.get(i).get(1) > maxLon) maxLon = geometry.coordinates.get(i).get(0);
            }
            return new Pair<>(points, new MapRect(minLat, minLon, maxLat, maxLon));
        } else
            return null;
    }

    public class PointLocal {
        public double lat;
        public double lon;
        public float accuracy;
        public boolean turnedFromRoute;
    }

    public class MapRect {
        public double minLat;
        public double minLon;
        public double maxLat;
        public double maxLon;

        public MapRect(double minLat, double minLon, double maxLat, double maxLon) {
            this.minLat = minLat;
            this.minLon = minLon;
            this.maxLat = maxLat;
            this.maxLon = maxLon;
        }
    }
}
