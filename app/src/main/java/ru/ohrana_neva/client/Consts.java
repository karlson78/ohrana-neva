package ru.ohrana_neva.client;

public interface Consts {
    String PERMISSION_FINE_LOCATION = "android.permission.ACCESS_FINE_LOCATION";
    String PERMISSION_COARSE_LOCATION = "android.permission.ACCESS_COARSE_LOCATION";
    String PERMISSION_NETWORK_STATE = "android.permission.ACCESS_NETWORK_STATE";
    String PERMISSION_WIFI_STATE = "android.permission.ACCESS_WIFI_STATE";
    String PERMISSION_WRITE_EXTERNAL_STORAGE = "android.permission.WRITE_EXTERNAL_STORAGE";

    int REQUEST_CODE_PERMISSION_LOCATION = 2;

    int FIRST_STORE_VERSION = 41;
    String downloadDirName = ".appdir";
    String downloadFileName = "app-release.apk";
    String downloadUrl = "/assets/latest.apk";

    //String SOCKET_CONNECTION_ADDR = "ws://178.57.218.181:4201/?login=#1#&password=#2#";
    //String JOURNAL_URL = "http://178.57.218.181:3002/";
    String SOCKET_CONNECTION_ADDR = "wss://neva-api.ohrana-neva.ru:4201/?login=#1#&password=#2#";
    String JOURNAL_URL = "https://neva-api.ohrana-neva.ru";
    String ROUTE_API_URL = "http://api.probki.net/";

    String ROUTE_API_KEY = "NEVAS-10-280119";

    int LOCATIONS_BUF_SIZE = 5;
    int LOCATIONS_TO_TURN = 3;
    int LOCATIONS_TO_UPDATE = 8;

    int STATUS_FREE = 1;
    int STATUS_PATROL = 2;
    int STATUS_RIDE = 3;
    int STATUS_ARRIVED = 4;
    int STATUS_COMMAND = 5;
    int STATUS_OTHER = 6;
    int STATUS_RIDE_DIFFICULT = 7;

    int RESULT_NORMAL = 1;
    int RESULT_HARD_TO_VIEW = 2;
    int RESULT_PENETRATION = 3;
    int RESULT_PERIMETER_FAULT = 4;

    int AUDIO_REQUEST = 100;

    int RIDE_DIFFICULT_LOCK_TIME = 30;

    int TYPE_SECURITY = 1;
    int TYPE_FIRE = 2;
    int TYPE_ALARM_BUTTON = 3;

    long UPDATE_INTERVAL = 5000;
    long FASTEST_INTERVAL = 5000;
}
