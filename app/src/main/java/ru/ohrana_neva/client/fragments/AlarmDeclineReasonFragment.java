package ru.ohrana_neva.client.fragments;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import java.lang.ref.WeakReference;

import ru.ohrana_neva.client.R;
import ru.ohrana_neva.client.data.ApiClient;
import ru.ohrana_neva.client.data.model.AlarmApplyMessage;
import ru.ohrana_neva.client.data.model.Message;
import ru.ohrana_neva.client.data.model.SosApplyMessage;

public class AlarmDeclineReasonFragment extends BaseDialogFragment {
    private WeakReference<Message> msgRef = null;

    public Dialog prepareDialog(final Context context, final int layoutId) {
        final Dialog dialog = super.prepareDialog(context, layoutId);

        Button btnBusy = (Button) dialog.findViewById(R.id.btn_busy);
        btnBusy.setOnClickListener(v -> {
            if (msgRef != null && msgRef.get() != null) {
                if (msgRef.get() instanceof SosApplyMessage) {
                    ((SosApplyMessage) msgRef.get()).description = context.getResources().getString(R.string.reason_busy);
                } else {
                    ((AlarmApplyMessage) msgRef.get()).description = context.getResources().getString(R.string.reason_busy);
                }
                ApiClient.getInstance(context).dialogEvent(layoutId, v.getId(), msgRef.get());
                dialog.dismiss();
            }
        });
        Button btnBroken = (Button) dialog.findViewById(R.id.btn_broken);
        btnBroken.setOnClickListener(v -> {
            if (msgRef != null && msgRef.get() != null) {
                if (msgRef.get() instanceof SosApplyMessage) {
                    ((SosApplyMessage) msgRef.get()).description = context.getResources().getString(R.string.reason_broken);
                } else {
                    ((AlarmApplyMessage) msgRef.get()).description = context.getResources().getString(R.string.reason_broken);
                }
                ApiClient.getInstance(context).dialogEvent(layoutId, v.getId(), msgRef.get());
                dialog.dismiss();
            }
        });
        Button btnPatrol = (Button) dialog.findViewById(R.id.btn_patrol);
        btnPatrol.setOnClickListener(v -> {
            if (msgRef != null && msgRef.get() != null) {
                if (msgRef.get() instanceof SosApplyMessage) {
                    ((SosApplyMessage) msgRef.get()).description = context.getResources().getString(R.string.reason_patrol);
                } else {
                    ((AlarmApplyMessage) msgRef.get()).description = context.getResources().getString(R.string.reason_patrol);
                }
                ApiClient.getInstance(context).dialogEvent(layoutId, v.getId(), msgRef.get());
                dialog.dismiss();
            }
        });
        Button btnOrder = (Button) dialog.findViewById(R.id.btn_order);
        btnOrder.setOnClickListener(v -> {
            if (msgRef != null && msgRef.get() != null) {
                if (msgRef.get() instanceof SosApplyMessage) {
                    ((SosApplyMessage) msgRef.get()).description = context.getResources().getString(R.string.reason_order);
                } else {
                    ((AlarmApplyMessage) msgRef.get()).description = context.getResources().getString(R.string.reason_order);
                }
                ApiClient.getInstance(context).dialogEvent(layoutId, v.getId(), msgRef.get());
                dialog.dismiss();
            }
        });
        Button btnOk = (Button) dialog.findViewById(R.id.btn_ok);
        btnOk.setOnClickListener(v -> {
            if (msgRef != null && msgRef.get() != null) {
                if (msgRef.get() instanceof SosApplyMessage) {
                    ((SosApplyMessage) msgRef.get()).description = ((EditText) dialog.findViewById(R.id.et_another_reason)).getText().toString();
                } else {
                    ((AlarmApplyMessage) msgRef.get()).description = ((EditText) dialog.findViewById(R.id.et_another_reason)).getText().toString();
                }
                ApiClient.getInstance(context).dialogEvent(layoutId, v.getId(), msgRef.get());
                dialog.dismiss();
            }
        });

        return dialog;
    }

    public void showDialog(Context context, Message msg) {
        msgRef = new WeakReference<Message>(msg);
        final Dialog dialog = prepareDialog(context, R.layout.dialogfragment_alarm_declined);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        dialog.show();
    }

    public class Description {
        public int reasonId;
        public String reasonString;
    }

    @Override
    public void refreshOnConfigChanged(Context context) {
        super.refreshOnConfigChanged(context);
        if (msgRef != null && msgRef.get() != null) {
            showDialog(context, msgRef.get());
        }
    }
}
