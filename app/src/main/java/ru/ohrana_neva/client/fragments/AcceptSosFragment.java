package ru.ohrana_neva.client.fragments;

import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.ref.WeakReference;

import ru.ohrana_neva.client.R;
import ru.ohrana_neva.client.data.ApiClient;
import ru.ohrana_neva.client.data.model.AlarmApplyMessage;
import ru.ohrana_neva.client.data.model.AlarmMessage;
import ru.ohrana_neva.client.data.model.Message;
import ru.ohrana_neva.client.data.model.SosApplyMessage;
import ru.ohrana_neva.client.data.model.SosMessage;

public class AcceptSosFragment extends BaseDialogFragment {
    private WeakReference<Message> msgRef = null;

    public Dialog prepareDialog(final Context context, final int layoutId) {
        final Dialog dialog = super.prepareDialog(context, layoutId);

        Button btnOk = (Button) dialog.findViewById(R.id.btn_ok);
        btnOk.setOnClickListener(v -> {
            if (msgRef.get() instanceof SosMessage) {
                SosApplyMessage msg = new SosApplyMessage();
                msg.action = "sosApply";
                msg.result = true;
                msg.description = "";
                msg.id = ((SosMessage) msgRef.get()).id;
                msg.lat = ((SosMessage) msgRef.get()).lat;
                msg.lon = ((SosMessage) msgRef.get()).lon;
                dialog.dismiss();
                ApiClient.getInstance(context).dialogEvent(layoutId, v.getId(), msg);
            } else {
                AlarmApplyMessage msg = new AlarmApplyMessage();
                msg.action = "alarmApply";
                msg.result = true;
                msg.description = "";
                msg.id = ((AlarmMessage) msgRef.get()).id;
                dialog.dismiss();
                ApiClient.getInstance(context).dialogEvent(layoutId, v.getId(), msg);
            }
        });
        Button btnCancel = (Button) dialog.findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(v -> {
            if (msgRef.get() instanceof SosMessage) {
                SosApplyMessage msg = new SosApplyMessage();
                msg.action = "sosApply";
                msg.result = false;
                msg.description = "";
                msg.id = ((SosMessage) msgRef.get()).id;
                msg.lat = ((SosMessage) msgRef.get()).lat;
                msg.lon = ((SosMessage) msgRef.get()).lon;
                dialog.dismiss();
                ApiClient.getInstance(context).dialogEvent(layoutId, v.getId(), msg);
            } else {
                AlarmApplyMessage msg = new AlarmApplyMessage();
                msg.action = "alarmApply";
                msg.result = true;
                msg.description = "";
                msg.id = ((AlarmMessage) msgRef.get()).id;
                dialog.dismiss();
                ApiClient.getInstance(context).dialogEvent(layoutId, v.getId(), msg);
            }
        });

        return dialog;
    }

    public void showDialog(Context context, Message msg) {
        msgRef = new WeakReference<Message>(msg);
        final Dialog dialog = prepareDialog(context, R.layout.dialogfragment_sos);
        ImageView ivSos = (ImageView) dialog.findViewById(R.id.img_sos);
        TextView tvAddress = (TextView) dialog.findViewById(R.id.tv_address);
        TextView tvName = (TextView) dialog.findViewById(R.id.tv_name);
        if (msg instanceof AlarmMessage) {
            ivSos.setImageResource(R.drawable.icon_alarm);
            tvAddress.setText(((AlarmMessage) msg).getText());
            tvName.setVisibility(View.VISIBLE);
            tvName.setText(((AlarmMessage) msg).name);
        } else
        if (msg instanceof SosMessage) {
            ivSos.setImageResource(R.drawable.icon_sos);
            if (!TextUtils.isEmpty(((SosMessage) msg).address)) {
                tvAddress.setText(((SosMessage) msg).address);
                tvName.setVisibility(View.VISIBLE);
            } else {
                tvName.setVisibility(View.GONE);
            }
            tvName.setVisibility(View.GONE);
        }
        dialog.show();
    }

    @Override
    public void refreshOnConfigChanged(Context context) {
        super.refreshOnConfigChanged(context);
        if (msgRef != null && msgRef.get() != null) {
            showDialog(context, msgRef.get());
        }
    }
}
