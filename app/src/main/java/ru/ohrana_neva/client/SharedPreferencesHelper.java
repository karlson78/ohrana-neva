package ru.ohrana_neva.client;

import android.content.Context;
import android.graphics.Color;

public class SharedPreferencesHelper {
    private static final String PREFERENCES = "preferences";

    private static final int MODE = Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS;

    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String IS_LOGGED_IN = "is_logged_in";
    public static final String BUTTONS_BG_COLOR = "buttonsbgcolor";
    public static final String SOS_BUTTON_BG_COLOR = "sosbuttonbgcolor";
    public static final String ROUTE_COLOR = "routecolor";
    public static final String SOUND_FILE_PATH = "soundfilepath";
    public static final String OPERATOR_PHONE = "operatorphone";

    public static String getUsername(Context context) {
        return context.getSharedPreferences(PREFERENCES, MODE).getString(USERNAME, "");
    }

    public static void setUsername(Context context, String value) {
        context.getSharedPreferences(PREFERENCES, MODE).edit().putString(USERNAME, value).commit();
    }

    public static String getPassword(Context context) {
        return context.getSharedPreferences(PREFERENCES, MODE).getString(PASSWORD, "");
    }

    public static void setPassword(Context context, String value) {
        context.getSharedPreferences(PREFERENCES, MODE).edit().putString(PASSWORD, value).commit();
    }

    public static boolean isLoggedIn(Context context) {
        return context.getSharedPreferences(PREFERENCES, MODE).getBoolean(IS_LOGGED_IN, false);
    }

    public static void setIsLoggedIn(Context context, boolean value) {
        context.getSharedPreferences(PREFERENCES, MODE).edit().putBoolean(IS_LOGGED_IN, value).commit();
    }

    public static int getButtonsBackgroundColor(Context context) {
        return context.getSharedPreferences(PREFERENCES, MODE).getInt(BUTTONS_BG_COLOR, context.getResources().getColor(R.color.colorButton));
    }

    public static void setButtonsBackgroundColor(Context context, int color) {
        context.getSharedPreferences(PREFERENCES, MODE).edit().putInt(BUTTONS_BG_COLOR, color).commit();
    }

    public static int getSOSButtonBackgroundColor(Context context) {
        return context.getSharedPreferences(PREFERENCES, MODE).getInt(SOS_BUTTON_BG_COLOR, context.getResources().getColor(R.color.colorRedButton));
    }

    public static void setSOSButtonBackgroundColor(Context context, int color) {
        context.getSharedPreferences(PREFERENCES, MODE).edit().putInt(SOS_BUTTON_BG_COLOR, color).commit();
    }

    public static int getRouteColor(Context context) {
        return context.getSharedPreferences(PREFERENCES, MODE).getInt(ROUTE_COLOR, context.getResources().getColor(R.color.colorLine));
    }

    public static void setRouteColor(Context context, int color) {
        context.getSharedPreferences(PREFERENCES, MODE).edit().putInt(ROUTE_COLOR, color).commit();
    }

    public static String getSoundFilePath(Context context) {
        return context.getSharedPreferences(PREFERENCES, MODE).getString(SOUND_FILE_PATH, "");
    }

    public static void setSoundFilePath(Context context, String path) {
        context.getSharedPreferences(PREFERENCES, MODE).edit().putString(SOUND_FILE_PATH, path).commit();
    }

    public static String getOperatorPhone(Context context) {
        return context.getSharedPreferences(PREFERENCES, MODE).getString(OPERATOR_PHONE, "");
    }

    public static void setOperatorPhone(Context context, String phone) {
        context.getSharedPreferences(PREFERENCES, MODE).edit().putString(OPERATOR_PHONE, phone).commit();
    }
}
