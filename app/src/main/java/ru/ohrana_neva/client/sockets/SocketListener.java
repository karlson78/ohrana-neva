package ru.ohrana_neva.client.sockets;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import com.google.gson.Gson;
import com.neovisionaries.ws.client.WebSocket;
import com.neovisionaries.ws.client.WebSocketAdapter;
import com.neovisionaries.ws.client.WebSocketException;
import com.neovisionaries.ws.client.WebSocketFactory;
import com.neovisionaries.ws.client.WebSocketFrame;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.ohrana_neva.client.Consts;
import ru.ohrana_neva.client.MainActivity;
import ru.ohrana_neva.client.R;
import ru.ohrana_neva.client.SharedPreferencesHelper;
import ru.ohrana_neva.client.data.ApiClient;
import ru.ohrana_neva.client.data.LocationsRecorder;
import ru.ohrana_neva.client.data.Messages;
import ru.ohrana_neva.client.data.model.AlarmApplyMessage;
import ru.ohrana_neva.client.data.model.AlarmMessage;
import ru.ohrana_neva.client.data.model.CityGuideNearestAddrResponse;
import ru.ohrana_neva.client.data.model.Message;
import ru.ohrana_neva.client.data.model.SosApplyMessage;
import ru.ohrana_neva.client.data.model.SosMessage;
import timber.log.Timber;

public class SocketListener extends WebSocketAdapter {
    private boolean connected = false;
    private boolean isFirstLogin = true;
    private WeakReference<Context> contextRef = null;
    private WebSocket ws = null;
    private Timer mTimer = null;
    private long locationDate;
    private long connectTime = 0;
    private long textMsgDate;
    private long sendEndStatusDate;
    private long sendIsFreeDate;
    private Handler mainHandler;

    public boolean isConnected() {
        return connected;
    }

    public void init(Context context) {
        if (MainActivity.getInstance() == null) return;
        WebSocketFactory factory = new WebSocketFactory();
        sendIsFreeDate = sendEndStatusDate = textMsgDate = locationDate = (new Date()).getTime();
        contextRef = new WeakReference<Context>(context);
        mainHandler = new Handler(Looper.getMainLooper());
        isFirstLogin = true;
        if (ws == null) {
            try {
                String connectionAddr = Consts.SOCKET_CONNECTION_ADDR;
                connectionAddr = connectionAddr.replace("#1#", SharedPreferencesHelper.getUsername(contextRef.get()).trim());
                connectionAddr = connectionAddr.replace("#2#", SharedPreferencesHelper.getPassword(contextRef.get()).trim());
                ws = factory.createSocket(connectionAddr, 5000);
                ws.addListener(this);
                ws.connectAsynchronously();
                ws.setPingInterval(10*1000);
            } catch (IOException e) {
                ws = null;
                Timber.e(e);
            }
        } else {
            try {
                ws = ws.recreate().connectAsynchronously();
                ws.setPingInterval(10*1000);
            } catch (IOException e) {
                Timber.e(e);
                ws = null;
                reconnect();
            }
        }
    }

    public void disconnect() {
        if (ws != null) {
            ws.disconnect();
        }

        cancelTimer();
    }

    public void reconnect() {
        if (contextRef != null && contextRef.get() != null && !((Activity) contextRef.get()).isFinishing()) {
            init(contextRef.get());
        }
    }

    public void cancelTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
    }

    public void sendMessage(String msg) {
        if (ws == null) return;
        ws.sendText(msg);
    }

    public void sendLatLon(double lat, double lon, float accuracy) {
        synchronized (this) {
            JSONObject obj = new JSONObject();
            JSONObject message = new JSONObject();
            if (ws == null) return;
            Date date = new Date();
            if (date.getTime() - locationDate <= 150) return;
            locationDate = date.getTime();
            LocationsRecorder.getInstance().addLocationDebug();
            try {
                obj.put("action", "track");
                message.put("lat", lat);
                message.put("lon", lon);
                message.put("accuracy", accuracy);
                if (MainActivity.getInstance() != null) {
                    message.put("status", MainActivity.getInstance().getStatus());
                }
                obj.put("message", message);
                ws.sendText(obj.toString());
            } catch (JSONException e) {
                Timber.e(e);
            }
        }
    }

    public void sendSos(double lat, double lon) {
        sendSos(lat, lon, false);
    }

    public void sendSos(double lat, double lon, boolean confirmed) {
        synchronized (this) {
            JSONObject obj = new JSONObject();
            JSONObject message = new JSONObject();
            if (ws == null) return;
            if (!connected) {
                String msg = "SOS! Экипаж: " + SharedPreferencesHelper.getUsername(contextRef.get()) + ", широта: " + LocationsRecorder.getInstance().getLastLocation().lat
                        + ", долгота: " + LocationsRecorder.getInstance().getLastLocation().lon;
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:" + SharedPreferencesHelper.getOperatorPhone(contextRef.get())));
                intent.putExtra("sms_body", msg);
                contextRef.get().startActivity(intent);

                return;
            }
            Date date = new Date();
            //if (date.getTime() - locationDate <= 150) return;
            locationDate = date.getTime();
            try {
                obj.put("action", "sos");
                message.put("lat", lat);
                message.put("lon", lon);
                if (confirmed) {
                    message.put("confirmed", true);
                }
                if (MainActivity.getInstance() != null) {
                    message.put("status", MainActivity.getInstance().getStatus());
                }
                obj.put("message", message);
                ws.sendText(obj.toString());
            } catch (JSONException e) {
                Timber.e(e);
            }
        }
    }

    public void sendDebugSosOrAlarm(double lat, double lon, String type) {
        synchronized (this) {
            JSONObject objWrap = new JSONObject();
            JSONObject obj = new JSONObject();
            JSONObject message = new JSONObject();
            if (ws == null) return;
            Date date = new Date();
            //if (date.getTime() - locationDate <= 150) return;
            locationDate = date.getTime();
            try {
                obj.put("action", type);
                message.put("lat", lat);
                message.put("lon", lon);
                if (MainActivity.getInstance() != null) {
                    message.put("status", MainActivity.getInstance().getStatus());
                }
                obj.put("message", message);
                objWrap.put("action", "customMessage");
                objWrap.put("message", obj);
                ws.sendText(objWrap.toString());
            } catch (JSONException e) {
                Timber.e(e);
            }
        }
    }

    public void sendSosOrAlarmApply(Message msg) {
        synchronized (this) {
            JSONObject obj = new JSONObject();
            JSONObject message = new JSONObject();
            if (ws == null) return;
            Date date = new Date();
            if (date.getTime() - textMsgDate <= 150) return;
            textMsgDate = date.getTime();
            try {
                if (msg instanceof SosApplyMessage) {
                    obj.put("action", "sosApply");
                    message.put("result", ((SosApplyMessage) msg).result);
                    message.put("description", ((SosApplyMessage) msg).description);
                    message.put("id", ((SosApplyMessage) msg).id);
                } else
                if (msg instanceof AlarmApplyMessage) {
                    obj.put("action", "alarmApply");
                    message.put("result", ((AlarmApplyMessage) msg).result);
                    message.put("description", ((AlarmApplyMessage) msg).description);
                    message.put("id", ((AlarmApplyMessage) msg).id);
                }
                if (MainActivity.getInstance() != null) {
                    message.put("status", MainActivity.getInstance().getStatus());
                }
                obj.put("message", message);
                ws.sendText(obj.toString());
            } catch (JSONException e) {
                Timber.e(e);
            }
        }
    }

    public void sendEndStatus(int status) {
        synchronized (this) {
            JSONObject obj = new JSONObject();
            JSONObject message = new JSONObject();
            if (ws == null || !connected) return;
            Date date = new Date();
            if (date.getTime() - sendEndStatusDate <= 150) return;
            sendEndStatusDate = date.getTime();
            try {
                obj.put("action", "endStatus");
                Message msg = ApiClient.getInstance(contextRef.get()).getLastAlarmOrSos();
                if (msg instanceof SosMessage) {
                    message.put("type", "sos");
                    message.put("id", ((SosMessage) msg).id);
                } else
                if (msg instanceof AlarmMessage) {
                    message.put("type", "alarm");
                    message.put("id", ((AlarmMessage) msg).id);
                }
                message.put("status", status);
                obj.put("message", message);
                ws.sendText(obj.toString());

                Toast.makeText(contextRef.get(), contextRef.get().getResources().getString(R.string.str_message_sent), Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Timber.e(e);
            }
        }
    }

    public void sendIsFree(String id) {
        synchronized (this) {
            JSONObject obj = new JSONObject();
            JSONObject message = new JSONObject();
            if (ws == null || !connected) return;
            Date date = new Date();
            if (date.getTime() - sendIsFreeDate <= 150) return;
            sendIsFreeDate = date.getTime();
            try {
                obj.put("action", "isFree");
                message.put("id", id);

                obj.put("message", message);
                ws.sendText(obj.toString());
            } catch (Exception e) {
                Timber.e(e);
            }
        }
    }

    public void sendInfo(String id, String text) {
        synchronized (this) {
            JSONObject obj = new JSONObject();
            JSONObject message = new JSONObject();
            if (ws == null || !connected) return;
            try {
                obj.put("action", "sendInfo");
                message.put("id", id);
                message.put("text", text);

                obj.put("message", message);
                ws.sendText(obj.toString());
            } catch (Exception e) {
                Timber.e(e);
            }
        }
    }

    @Override
    public void onConnected(WebSocket websocket, Map<String, List<String>> headers) throws Exception {
        mainHandler.post(() -> {
            if (contextRef != null && contextRef.get() != null && !((Activity) contextRef.get()).isFinishing()) {
                //System.out.println("...connected to: " + websocket.getURI() + " " + websocket.getState().toString() + " " + connectTime + " " + System.currentTimeMillis());
                if (!connected) {
                    connected = true;
                    cancelTimer();
                }
                if (System.currentTimeMillis() - connectTime <= 5000 && isFirstLogin) {
                    if (contextRef != null && contextRef.get() != null) {
                        //SharedPreferencesHelper.setUsername(contextRef.get(), "");
                        //SharedPreferencesHelper.setPassword(contextRef.get(), "");
                        SharedPreferencesHelper.setIsLoggedIn(contextRef.get(), false);
                        Toast.makeText(contextRef.get(), contextRef.get().getResources().getString(R.string.str_login_error), Toast.LENGTH_SHORT).show();
                        ((Activity) contextRef.get()).finish();
                    }
                }
                connectTime = System.currentTimeMillis();
                isFirstLogin = false;
            }
        });
    }

    @Override
    public void onConnectError(WebSocket websocket, WebSocketException exception) throws Exception {
        connected = false;
        System.out.println(exception.getMessage());
        mainHandler.post(() -> {
            if (contextRef != null && contextRef.get() != null && !((Activity) contextRef.get()).isFinishing()) {
                ws = null;
                if (mTimer == null) {
                    mTimer = new Timer();
                    mTimer.schedule(new TimerTaskToReconnect(), 5, 10000);
                }
            } else {

            }
        });
    }

    @Override
    public void onDisconnected(WebSocket websocket,
                               WebSocketFrame serverCloseFrame, WebSocketFrame clientCloseFrame,
                               boolean closedByServer) throws Exception {
        connected = false;
        mainHandler.post(() -> doReconnect());
    }

    @Override
    public void onPingFrame(WebSocket websocket, WebSocketFrame frame) throws Exception {
        connected = true;
    }

    @Override
    public void onPongFrame(WebSocket websocket, WebSocketFrame frame) throws Exception {
        connected = true;
    }

    @Override
    public void onTextMessage(WebSocket websocket, String message) throws Exception {
        if (!connected) {
            connected = true;
        }
        //System.out.println("...message: " + message);
        Date date = new Date();
        if (date.getTime() - textMsgDate <= 150) return;
        textMsgDate = date.getTime();
        if (contextRef != null && contextRef.get() != null) {
            mainHandler.post(() -> {
                Message msg = ApiClient.getInstance(contextRef.get()).getGson().fromJson(message, Message.class);
                if (msg instanceof SosMessage) {
                    ApiClient.getInstance(contextRef.get()).getNearestAddr(((SosMessage) msg).lat, ((SosMessage) msg).lon)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(response -> onAddressResponseSuccess(response, (SosMessage) msg),
                                    t -> onAddressResponseError((SosMessage) msg));
                } else {
                    ApiClient.getInstance(contextRef.get()).getSosSubject().onNext(msg);
                }
            });
        }
    }

    private void onAddressResponseSuccess(CityGuideNearestAddrResponse response, SosMessage msg) {
        if (response.addresses != null && response.addresses.size() > 0) {
            msg.address = response.addresses.get(0).city + ", "
                    + response.addresses.get(0).street
                    + response.addresses.get(0).house;
        }
        ApiClient.getInstance(contextRef.get()).getSosSubject().onNext(msg);
    }

    private void onAddressResponseError(SosMessage msg) {
        ApiClient.getInstance(contextRef.get()).getSosSubject().onNext(msg);
    }

    @Override
    public void onUnexpectedError(WebSocket websocket, WebSocketException cause) throws Exception {
        connected = false;
        mainHandler.post(() -> doReconnect());
    }

    private void doReconnect() {
        if (contextRef != null && contextRef.get() != null && !((Activity) contextRef.get()).isFinishing()) {
            ws = null;
            if (mTimer == null) {
                mTimer = new Timer();
                mTimer.schedule(new TimerTaskToReconnect(), 5, 10000);
            }
            reconnect();
        } else {
            cancelTimer();
        }
    }

    private class TimerTaskToReconnect extends TimerTask {
        @Override
        public void run() {
            mainHandler.post(() -> {
                if (!connected) {
                    reconnect();
                }
            });
        }
    }
}
