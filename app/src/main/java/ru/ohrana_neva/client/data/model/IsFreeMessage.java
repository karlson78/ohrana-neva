package ru.ohrana_neva.client.data.model;

import android.os.Parcel;

public class IsFreeMessage extends Message {
    public int message;

    public IsFreeMessage() {}

    protected IsFreeMessage(Parcel in) {
        action = in.readString();
        message = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(action);
        dest.writeInt(message);
    }

    public static final Creator<IsFreeMessage> CREATOR = new Creator<IsFreeMessage>() {
        @Override
        public IsFreeMessage createFromParcel(Parcel in) {
            return new IsFreeMessage(in);
        }

        @Override
        public IsFreeMessage[] newArray(int size) {
            return new IsFreeMessage[size];
        }
    };
}
