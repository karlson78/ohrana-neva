package ru.ohrana_neva.client.versionupdater;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.BuildConfig;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.widget.Toast;

import org.reactivestreams.Subscriber;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.http.GET;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

public class VersionUpdater {
    private static final String TAG = "DownloadAPI";
    private static final int DEFAULT_TIMEOUT = 15;
    private static final int BUFFER_SIZE = 4096;

    public Retrofit retrofit;

    public VersionUpdater(String url, DownloadProgressListener listener) {

        DownloadProgressInterceptor interceptor = new DownloadProgressInterceptor(listener);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .retryOnConnectionFailure(true)
                .connectTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS)
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    public Observable<InputStream> downloadAPK(@NonNull String url, final File file) {
        Log.d(TAG, "downloadAPK: " + url);

        return retrofit.create(DownloadService.class)
                .download(url)
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .map(responseBody ->  responseBody.byteStream())
                .observeOn(Schedulers.computation())
                .doOnNext(inputStream -> {
                    writeFile(inputStream, file);
                })
                .observeOn(AndroidSchedulers.mainThread());
    }

    public interface DownloadService {
        @Streaming
        @GET
        Observable<ResponseBody> download(@Url String url);
    }

    public static boolean canRunRootCommands() {
        boolean retval;
        Process suProcess;

        try {
            suProcess = Runtime.getRuntime().exec("su");

            DataOutputStream os = new DataOutputStream(suProcess.getOutputStream());
            DataInputStream osRes = new DataInputStream(suProcess.getInputStream());

            // Getting the id of the current user to check if this is root
            os.writeBytes("id\n");
            os.flush();

            String currUid = osRes.readLine();
            boolean exitSu;
            if (null == currUid) {
                retval = false;
                exitSu = false;
                System.out.println("ROOT: Can't get root access or denied by user");
            } else if (currUid.contains("uid=0")) {
                retval = true;
                exitSu = true;
                System.out.println("ROOT: Root access granted");
            } else {
                retval = false;
                exitSu = true;
                System.out.println("ROOT: Root access rejected: " + currUid);
            }

            if (exitSu) {
                os.writeBytes("exit\n");
                os.flush();
            }
        } catch (Exception e) {
            // Can't get root !
            // Probably broken pipe exception on trying to write to output stream (os) after su failed, meaning that the device is not rooted

            retval = false;
            System.out.println("ROOT: Root access rejected [" + e.getClass().getName() + "] : " + e.getMessage());
        }

        return retval;
    }

    private void execute_as_root( String[] commands ) {
        try {
            Process p = Runtime.getRuntime().exec( "su" );
            InputStream es = p.getErrorStream();
            DataOutputStream os = new DataOutputStream(p.getOutputStream());

            for( String command : commands ) {
                os.writeBytes(command + "\n");
            }
            os.writeBytes("exit\n");
            os.flush();
            os.close();

            int read;
            byte[] buffer = new byte[4096];
            String output = new String();
            while ((read = es.read(buffer)) > 0) {
                output += new String(buffer, 0, read);
            }

            p.waitFor();
            System.out.println(output.trim() + " (" + p.exitValue() + ")");
        } catch (IOException e) {
            System.out.println("IOException: " + e.getMessage());
        } catch (InterruptedException e) {
            System.out.println("InterruptedException" + e.getMessage());
        }
    }

    private String get_main_activity(Context context) {
        PackageManager pm = context.getPackageManager();
        String packageName = context.getPackageName();

        try {
            final int flags = PackageManager.GET_ACTIVITIES;
            PackageInfo packageInfo = pm.getPackageInfo(packageName, flags);
            for( ActivityInfo ai : packageInfo.activities ) {
                if( ai.exported ) {
                    return ai.name;
                }
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("get_main_activity() failed");
        return "";
    }

    public void doSilentUpdate(Activity activity, String filename) {
        if( filename.length() > 0) {
            final String libs = "LD_LIBRARY_PATH=/vendor/lib:/system/lib ";
            final String[] commands = {
                    libs + "pm install -r " + filename,
                    libs + "am start -n " + activity.getPackageName() + "/" + get_main_activity(activity)
            };
            execute_as_root(commands);	// not supposed to return if successful
            final Activity acFinal = activity;
            if (acFinal != null) {
                acFinal.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(acFinal, "Будет запущена новая версия приложения...", Toast.LENGTH_SHORT).show();
                        acFinal.finish();
                    }
                });
            }
        }
    }

    public void doVerboseUpdate(Activity activity, String filename) {
        Intent install;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Uri apkUri = FileProvider.getUriForFile(activity,
                    activity.getPackageName() + ".provider",
                    new File(filename));
            install = new Intent(Intent.ACTION_INSTALL_PACKAGE);
            install.setData(apkUri);
            install.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        } else {
            Uri uri = Uri.fromFile(new File(filename));
            install = new Intent(Intent.ACTION_VIEW);
            if (Build.VERSION.SDK_INT >= 14) {
                install.putExtra(Intent.EXTRA_INSTALLER_PACKAGE_NAME, activity.getPackageName());
            }
            install.setDataAndType(uri, "application/vnd.android.package-archive");
            install.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        activity.startActivity(install);
    }

    private void writeFile(InputStream inputStream, File file) {
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(file);
            int read;
            byte[] buffer = new byte[BUFFER_SIZE];
            while ((read = inputStream.read(buffer)) != -1) {
                fileOutputStream.write(buffer, 0, read);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
