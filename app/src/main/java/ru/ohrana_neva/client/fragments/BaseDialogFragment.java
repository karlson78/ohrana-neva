package ru.ohrana_neva.client.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import java.lang.ref.WeakReference;

import io.reactivex.disposables.Disposable;
import ru.ohrana_neva.client.R;
import ru.ohrana_neva.client.SharedPreferencesHelper;
import ru.ohrana_neva.client.Utils;
import ru.ohrana_neva.client.data.ApiClient;

public class BaseDialogFragment implements View.OnClickListener {
    protected WeakReference<Dialog> dialogRef;
    protected WeakReference<Context> contextRef;
    protected WeakReference<View> viewRef;
    protected Disposable bgColorDisposable = null;
    protected Disposable sosBgColorDisposable = null;
    protected int layoutId;

    private static BaseDialogFragment instance = null;

    public static BaseDialogFragment getInstance() {
        return instance;
    }

    public Dialog prepareDialog(Context context, int layoutId) {
        instance = this;
        this.layoutId = layoutId;
        final Dialog dialog = new Dialog(context, android.R.style.Theme_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LayoutInflater.from(context).inflate(layoutId, null);
        dialog.setContentView(view);
        dialog.setOnDismissListener(dialog1 -> {
            if (bgColorDisposable != null) {
                bgColorDisposable.dispose();
                bgColorDisposable = null;
            }
            if (sosBgColorDisposable != null) {
                sosBgColorDisposable.dispose();
                sosBgColorDisposable = null;
            }
            instance = null;
        });

        prepareButtons(view);

        Window window = dialog.getWindow();
        if (window == null) {
            return dialog;
        }

        window.setGravity(Gravity.CENTER);
        int margin = context.getResources().getDimensionPixelSize(R.dimen.margin_xlarge);
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        int screenWidth = metrics.widthPixels;
        window.setLayout(screenWidth - 2 * margin, window.getAttributes().height);

        dialogRef = new WeakReference<Dialog>(dialog);
        contextRef = new WeakReference<Context>(context);
        viewRef = new WeakReference<View>(view);

        bgColorDisposable = ApiClient.getInstance(context).getBgColorSubject().subscribe(color -> changeButtonColors(viewRef.get(), color));
        sosBgColorDisposable = ApiClient.getInstance(context).getSosBgColorSubject().subscribe(color -> changeSosButtonColor(viewRef.get(), color));

        dialog.setOnDismissListener(dialogInterface -> {

        });
        return dialog;
    }

    public Dialog getDialog() {
        return ((dialogRef == null) ? null : dialogRef.get());
    }

    protected void prepareButtons(View view) {
        if (!(view instanceof ViewGroup)) {
            if (view instanceof Button) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    view.setBackgroundResource(R.drawable.btn_v21_large_radius);
                    view.setStateListAnimator(null);
                } else {
                    view.setBackgroundResource(R.drawable.btn_rounded_large_radius);
                }
                Utils.setButtonColors((Button) view, SharedPreferencesHelper.getButtonsBackgroundColor(view.getContext()), Utils.getHighlightedColor(SharedPreferencesHelper.getButtonsBackgroundColor(view.getContext())));
            }
            return;
        }
        ViewGroup viewGroup = (ViewGroup) view;
        for (int i = 0; i < viewGroup.getChildCount(); i++) {
            prepareButtons(viewGroup.getChildAt(i));
        }
    }

    @Override
    public void onClick(View v) {

    }

    protected void changeButtonColors(View view, int color) {
        if (!(view instanceof ViewGroup)) {
            if (view instanceof Button) {
                Utils.setButtonColors((Button) view, color, Utils.getHighlightedColor(color));
            }
            return;
        }
        ViewGroup viewGroup = (ViewGroup) view;
        for (int i = 0; i < viewGroup.getChildCount(); i++) {
            changeButtonColors(viewGroup.getChildAt(i), color);
        }
    }

    protected void changeSosButtonColor(View view, int color) {

    }

    public void refreshOnConfigChanged(Context context) {
        if (dialogRef != null && dialogRef.get() != null) {
            dialogRef.get().dismiss();
        }
    }
}