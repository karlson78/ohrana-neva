package ru.ohrana_neva.client.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.ohrana_neva.client.MainActivity;
import ru.ohrana_neva.client.R;
import ru.ohrana_neva.client.SharedPreferencesHelper;
import ru.ohrana_neva.client.data.ApiClient;
import ru.ohrana_neva.client.data.model.JournalRecord;

public class JournalAdapter extends RecyclerView.Adapter<JournalAdapter.VH> {
    private List<JournalRecord> data = null;
    private WeakReference<Context> contextRef = null;

    public void setData(List<JournalRecord> data) {
        if (this.data != null) {
            this.data.clear();
        } else {
            this.data = new ArrayList<>();
        }
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (contextRef == null || contextRef.get() == null) {
            contextRef = new WeakReference<Context>(parent.getContext());
        }
        View itemView = inflater.inflate(R.layout.journal_item, parent, false);
        return new JournalAdapter.VH(itemView);
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {
        holder.bind(data.get(position));
    }

    @Override
    public int getItemCount() {
        return (data == null) ? 0 : data.size();
    }

    public class VH extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_icon)
        ImageView ivIcon;
        @BindView(R.id.btn_more)
        Button btnMore;
        @BindView(R.id.tv_alarm_time)
        TextView tvAlarmTime;
        @BindView(R.id.tv_alarm_address)
        TextView tvAlarmAddress;
        @BindView(R.id.tv_alarm_name)
        TextView tvAlarmName;
        @BindView(R.id.tv_alarm_operator)
        TextView tvAlarmOperator;
        @BindView(R.id.tv_alarm_brigade)
        TextView tvAlarmBrigade;

        public VH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(JournalRecord rec) {
            final String recId = rec.id;

            if (!TextUtils.isEmpty(rec.action) && rec.action.toUpperCase().contains("SOS")) {
                ivIcon.setImageResource(R.drawable.icon_sos);
            } else {
                ivIcon.setImageResource(R.drawable.icon_alarm);
            }
            SimpleDateFormat format = new SimpleDateFormat("dd MMMM yyyy HH:mm:ss");
            if (rec.created != null) {
                Date date = new Date();
                date.setTime(rec.created_timestamp*1000);
                tvAlarmTime.setText(format.format(date));
            } else {
                tvAlarmTime.setText("-");
            }
            if (rec.params != null && !TextUtils.isEmpty(rec.params.address)) {
                String addr = rec.params.address;
                if (rec.params.pult_id != null) {
                    addr = addr + ", пультовый номер " + rec.params.pult_id;
                } else
                if (rec.params.number != null) {
                    addr = addr + ", пультовый номер " + rec.params.number;
                }
                tvAlarmAddress.setText(addr);
            } else {
                tvAlarmAddress.setText("-");
            }
            if (rec.params != null && !TextUtils.isEmpty(rec.params.name)) {
                tvAlarmName.setVisibility(View.VISIBLE);
                tvAlarmName.setText(rec.params.name);
            } else {
                tvAlarmName.setVisibility(View.GONE);
                tvAlarmName.setText("");
            }
            if (!TextUtils.isEmpty(rec.operator_name)) {
                tvAlarmOperator.setText(rec.operator_name);
            } else {
                tvAlarmOperator.setText("-");
            }
            if (!TextUtils.isEmpty(rec.brigade_name)) {
                tvAlarmBrigade.setText(rec.brigade_name);
            } else {
                tvAlarmBrigade.setText("-");
            }

            btnMore.setOnClickListener(v -> {
                ApiClient.getInstance(contextRef.get()).getJournalService().getActionInfo(
                        SharedPreferencesHelper.getUsername(contextRef.get()),
                        SharedPreferencesHelper.getPassword(contextRef.get()),
                        recId
                )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(res -> {
                    if (contextRef != null && contextRef.get() != null) {
                        ((MainActivity) contextRef.get()).showActionsPopup(res.items, v);
                    }
                }, t -> {});
            });
        }
    }
}
