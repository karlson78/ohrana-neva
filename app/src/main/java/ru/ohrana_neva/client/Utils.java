package ru.ohrana_neva.client;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.RippleDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.widget.Button;

import org.osmdroid.tileprovider.constants.OpenStreetMapTileProviderConstants;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.tileprovider.tilesource.XYTileSource;
import org.osmdroid.views.MapView;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Utils {
    public static void setButtonColors(Button button, int normalColor, int highlightedColor) {
        InsetDrawable insetDrawable = (InsetDrawable) button.getBackground();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int stateEnabled = android.R.attr.state_enabled;

            RippleDrawable bgDrawable = (RippleDrawable) insetDrawable.getDrawable();
            ColorStateList colorStateList = new ColorStateList(
                    new int[][]
                            {
                                    new int[]{}
                            },
                    new int[]
                            {
                                    highlightedColor
                            }
            );
            bgDrawable.setColor(colorStateList);
            StateListDrawable stateListDrawable = (StateListDrawable) bgDrawable.findDrawableByLayerId(R.id.bg_selector);
            try {
                Method getStateDrawableIndex = StateListDrawable.class.getMethod("getStateDrawableIndex", int[].class);
                Method getStateDrawable = StateListDrawable.class.getMethod("getStateDrawable", int.class);
                int indexDisabled = (int) getStateDrawableIndex.invoke(stateListDrawable, new int[]{-stateEnabled});
                if (indexDisabled >= 0) {
                    GradientDrawable disabled = (GradientDrawable) getStateDrawable.invoke(stateListDrawable, indexDisabled);
                    if (disabled != null) {
                        disabled.setColor(highlightedColor);
                    }
                }
                int indexNormal = (int) getStateDrawableIndex.invoke(stateListDrawable, new int[]{stateEnabled});
                if (indexNormal >= 0) {
                    GradientDrawable normal = (GradientDrawable) getStateDrawable.invoke(stateListDrawable, indexNormal);
                    if (normal != null) {
                        normal.setColor(normalColor);
                    }
                }
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        } else {
            int stateFocused = android.R.attr.state_focused;
            int statePressed = android.R.attr.state_pressed;
            int stateEnabled = android.R.attr.state_enabled;

            StateListDrawable stateListDrawable = (StateListDrawable) button.getBackground();
            try {
                Method getStateDrawableIndex = StateListDrawable.class.getMethod("getStateDrawableIndex", int[].class);
                Method getStateDrawable = StateListDrawable.class.getMethod("getStateDrawable", int.class);
                int indexFocused = (int) getStateDrawableIndex.invoke(stateListDrawable, new int[]{stateFocused});
                if (indexFocused >= 0) {
                    InsetDrawable focused = (InsetDrawable) getStateDrawable.invoke(stateListDrawable, indexFocused);
                    if (focused != null) {
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                            GradientDrawable gd = (GradientDrawable) focused.getDrawable();
                            gd.setColor(highlightedColor);
                        }
                    }
                }
                int indexPressed = (int) getStateDrawableIndex.invoke(stateListDrawable, new int[]{statePressed});
                if (indexPressed >= 0) {
                    InsetDrawable pressed = (InsetDrawable) getStateDrawable.invoke(stateListDrawable, indexPressed);
                    if (pressed != null) {
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                            GradientDrawable gd = (GradientDrawable) pressed.getDrawable();
                            gd.setColor(highlightedColor);
                        }
                    }
                }
                int indexNormal = (int) getStateDrawableIndex.invoke(stateListDrawable, new int[]{-stateFocused, -statePressed, stateEnabled});
                if (indexNormal >= 0) {
                    InsetDrawable normal = (InsetDrawable) getStateDrawable.invoke(stateListDrawable, indexNormal);
                    if (normal != null) {
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                            GradientDrawable gd = (GradientDrawable) normal.getDrawable();
                            gd.setColor(highlightedColor);
                        }
                    }
                }
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }

    public static int getHighlightedColor(int color) {
        int r = 128 + Color.red(color)/2;
        int g = 128 + Color.green(color)/2;
        int b = 128 + Color.blue(color)/2;;
        return Color.rgb(r, g, b);
    }

    public static void initMap(MapView mapView, Context context) {
        mapView.setClickable(true);
        mapView.setBuiltInZoomControls(true);

        mapView.getController().setZoom(15); //set initial zoom-level, depends on your need
        //mapView.setTileSource(TileSourceFactory.DEFAULT_TILE_SOURCE);

        mapView.setTileSource(new XYTileSource("Mapnik", // name of the main file that has been zipped
                5,        // min map zoom level
                18,       // max map zoom level
                256,      // tile size pixels
                ".png",
                new String[]{"http://api.probki.net/map/"}));
    }
}
