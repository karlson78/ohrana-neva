package ru.ohrana_neva.client.fragments;

import android.app.Dialog;
import android.content.Context;
import android.widget.Button;
import android.widget.EditText;

import ru.ohrana_neva.client.R;
import ru.ohrana_neva.client.data.ApiClient;
import ru.ohrana_neva.client.data.model.AlarmApplyMessage;
import ru.ohrana_neva.client.data.model.SosApplyMessage;

public class ReportFragment extends BaseDialogFragment {
    public Dialog prepareDialog(final Context context, final int layoutId) {
        final Dialog dialog = super.prepareDialog(context, layoutId);

        Button btnGone = (Button) dialog.findViewById(R.id.btn_gone);
        btnGone.setEnabled(false);
        btnGone.setOnClickListener(v -> {
            ApiClient.getInstance(context).dialogEvent(layoutId, v.getId(), null);
            dialog.dismiss();
        });
        Button btnNormal = (Button) dialog.findViewById(R.id.btn_normal);
        btnNormal.setOnClickListener(v -> {
            ApiClient.getInstance(context).dialogEvent(layoutId, v.getId(), null);
            btnGone.setEnabled(true);
        });
        Button btnViewProblems = (Button) dialog.findViewById(R.id.btn_view_problems);
        btnViewProblems.setOnClickListener(v -> {
            ApiClient.getInstance(context).dialogEvent(layoutId, v.getId(), null);
            btnGone.setEnabled(true);
        });
        Button btnPenetration = (Button) dialog.findViewById(R.id.btn_penetration);
        btnPenetration.setOnClickListener(v -> {
            ApiClient.getInstance(context).dialogEvent(layoutId, v.getId(), null);
            btnGone.setEnabled(true);
        });
        Button btnPerimeterFault = (Button) dialog.findViewById(R.id.btn_perimeter_fault);
        btnPerimeterFault.setOnClickListener(v -> {
            ApiClient.getInstance(context).dialogEvent(layoutId, v.getId(), null);
            btnGone.setEnabled(true);
        });

        return dialog;
    }

    public void showDialog(Context context) {
        final Dialog dialog = prepareDialog(context, R.layout.dialogfragment_report);
        dialog.show();
    }

    @Override
    public void refreshOnConfigChanged(Context context) {
        super.refreshOnConfigChanged(context);
        showDialog(context);
    }
}
