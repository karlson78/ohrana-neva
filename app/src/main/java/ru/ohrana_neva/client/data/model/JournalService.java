package ru.ohrana_neva.client.data.model;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface JournalService {
    @Headers({
            "Content-type: application/json"
    })
    @GET("journal")
    Observable<List<JournalRecord>> getJournal(@Query("login") String login, @Query("password") String password);

    @Headers({
            "Content-type: application/json"
    })
    @GET("action-info")
    Observable<ActionRecord.ActionsResponse> getActionInfo(@Query("login") String login, @Query("password") String password, @Query("id") String alarmId);

    @GET("apk/get-version")
    Observable<String> getVersionFromStore();
}
