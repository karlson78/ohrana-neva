package ru.ohrana_neva.client.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CityGuideNearestAddrResponse {
    @SerializedName("result")
    public String result;
    @SerializedName("addresses")
    public List<Address> addresses;

    public static class Address {
        @SerializedName("city")
        public String city;
        @SerializedName("street")
        public String street;
        @SerializedName("house")
        public String house;
    }
}
