package ru.ohrana_neva.client.data;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import ru.ohrana_neva.client.App;
import ru.ohrana_neva.client.BuildConfig;
import ru.ohrana_neva.client.Consts;
import ru.ohrana_neva.client.R;
import ru.ohrana_neva.client.data.model.CityGuideNearestAddrResponse;
import ru.ohrana_neva.client.data.model.CityGuideRouteResponse;
import ru.ohrana_neva.client.data.model.JournalService;
import ru.ohrana_neva.client.data.model.Message;

public class ApiClient {
    private static volatile ApiClient instance = null;
    private Gson gson;
    private final RoutingService routingService;
    private final JournalService journalService;

    private WeakReference<Context> mContext;

    private GoogleApiClient googleApiClient;
    private LocationsHandler locationsHandler;

    private double lat;
    private double lon;
    private boolean alarmAccepted;
    private boolean confirmSos;

    private Message lastAlarmOrSos = null;
    private Message lastIsFree = null;

    private PublishSubject<DialogEventInfo> dialogEventsSubject = null;
    private PublishSubject<Message> sosSubject = null;
    private PublishSubject<Integer> bgColorSubject = null;
    private PublishSubject<Integer> sosBgColorSubject = null;
    private PublishSubject<Integer> routeColorSubject = null;

    public static ApiClient getInstance(Context context) {
        ApiClient localRef = instance;
        if (localRef == null) {
            synchronized (ApiClient.class) {
                localRef = new ApiClient(context);
                instance = localRef;
            }
        }
        return localRef;
    }

    public static void clear() {
        if (instance != null) {
            instance.alarmAccepted = false;
            instance.confirmSos = false;
            instance.dialogEventsSubject = null;
            instance.sosSubject = null;
            instance = null;
        }
    }

    public ApiClient(Context context) {
        mContext = new WeakReference<Context>(context);

        HttpLoggingInterceptor httpLoggingInterceptor = null;
        if (BuildConfig.DEBUG) {
            httpLoggingInterceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
        } else {
            httpLoggingInterceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.NONE);
        }

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .pingInterval(10, TimeUnit.SECONDS)
                .addInterceptor(httpLoggingInterceptor)
                .build();
        gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .registerTypeAdapter(Date.class, new DateDeserializer())
                .registerTypeAdapter(Message.class, new MessageDeserializer())
                .create();

        Retrofit retrofitRouting = new Retrofit.Builder()
                .baseUrl(Consts.ROUTE_API_URL)
                .client(okHttpClient)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        routingService = retrofitRouting.create(RoutingService.class);

        Retrofit retrofitJournal = new Retrofit.Builder()
                .baseUrl(Consts.JOURNAL_URL)
                .client(okHttpClient)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        journalService = retrofitJournal.create(JournalService.class);

        alarmAccepted = false;
        confirmSos = false;

        dialogEventsSubject = PublishSubject.create();
        sosSubject = PublishSubject.create();
        bgColorSubject = PublishSubject.create();
        sosBgColorSubject = PublishSubject.create();
        routeColorSubject = PublishSubject.create();
    }

    public GoogleApiClient getGoogleApiClient() {
        return googleApiClient;
    }

    public LocationsHandler getLocationsHandler() {
        return locationsHandler;
    }

    public RoutingService getRoutingService() {
        return routingService;
    }

    public JournalService getJournalService() {
        return journalService;
    }

    public PublishSubject<DialogEventInfo> getDialogEventsSubject() {
        return dialogEventsSubject;
    }

    public PublishSubject<Message> getSosSubject() {
        return sosSubject;
    }

    public PublishSubject<Integer> getBgColorSubject() {
        return bgColorSubject;
    }

    public PublishSubject<Integer> getSosBgColorSubject() {
        return sosBgColorSubject;
    }

    public PublishSubject<Integer> getRouteColorSubject() {
        return routeColorSubject;
    }

    public Message getLastAlarmOrSos() {
        return lastAlarmOrSos;
    }

    public void setLastAlarmOrSos(Message msg) {
        lastAlarmOrSos = msg;
    }

    public Message getLastIsFree() {
        return lastIsFree;
    }

    public void setLastIsFree(Message msg) {
        lastIsFree = msg;
    }

    public boolean isSosConfirmed() {
        return confirmSos;
    }

    public void setConfirmSos(boolean flag) {
        confirmSos = flag;
    }

    public void dialogEvent(int layoutId, int viewId, Object data) {
        DialogEventInfo info = new DialogEventInfo();
        info.layoutId = layoutId;
        info.viewId = viewId;
        info.data = data;
        dialogEventsSubject.onNext(info);
    }

    public Observable<CityGuideRouteResponse> getRoute(double lat1, double lon1, double lat2, double lon2) {
        JSONObject body = new JSONObject();
        try {
            JSONObject params = new JSONObject();
            body.put("accessKey", Consts.ROUTE_API_KEY);
            body.put("getPoints", true);
            params.put("type", "OptimizeTime");
            params.put("allowYards", false);
            params.put("allowSideWays", false);
            params.put("allowGroundRoads", true);
            params.put("allowPaidRoads", false);
            params.put("useLimitsUpd", false);
            params.put("useForecast", true);
            params.put("useJams", true);
            JSONObject point1 = new JSONObject();
            point1.put("latitude", lat1);
            point1.put("longitude", lon1);
            JSONObject point2 = new JSONObject();
            point2.put("latitude", lat2);
            point2.put("longitude", lon2);
            JSONArray array = new JSONArray();
            array.put(point1);
            array.put(point2);
            JSONObject pointsObject = new JSONObject();
            pointsObject.put("points", array);
            JSONArray routesArray = new JSONArray();
            routesArray.put(pointsObject);
            body.put("parameters", params);
            body.put("routes", routesArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return routingService.getRoute(body.toString());
    }

    public Observable<CityGuideNearestAddrResponse> getNearestAddr(double lat, double lon) {
        JSONObject body = new JSONObject();
        try {
            body.put("AccessKey", Consts.ROUTE_API_KEY);
            JSONObject center = new JSONObject();
            center.put("lat", lat);
            center.put("lon", lon);
            body.put("Center", center);
            body.put("DistanceInKm", 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return routingService.getNearestAddr(body.toString());
    }

    public double getLatitude() {
        return lat;
    }

    public void setLatitude(double lat) {
        this.lat = lat;
    }

    public double getLongitude() {
        return lon;
    }

    public void setLongitude(double lon) {
        this.lon = lon;
    }

    public boolean isAlarmAccepted() {
        return alarmAccepted;
    }

    public void setAlarmAccepted(boolean flag) {
        alarmAccepted = flag;
    }

    public Gson getGson() {
        return gson;
    }

    public void initLocationsApi(LocationsHandler.OnGoogleApiConnectedListener listener) {
        if (googleApiClient == null && locationsHandler == null) {
            locationsHandler = new LocationsHandler();
            googleApiClient = new GoogleApiClient.Builder(mContext.get()).
                    addApi(LocationServices.API).
                    addConnectionCallbacks(locationsHandler).
                    addOnConnectionFailedListener(locationsHandler).build();
        }
        locationsHandler.setListener(listener);
    }

    public void startLocationUpdates() {
        AppCompatActivity activity = (AppCompatActivity) locationsHandler.getListener();
        if (activity == null) return;

        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(Consts.UPDATE_INTERVAL);
        locationRequest.setFastestInterval(Consts.FASTEST_INTERVAL);

        if (ActivityCompat.checkSelfPermission(activity,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                &&  ActivityCompat.checkSelfPermission(activity,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(activity, R.string.read_gps_message, Toast.LENGTH_SHORT).show();
        }

        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, locationsHandler);
    }

    public class DateDeserializer implements JsonDeserializer<Date> {

        @Override
        public Date deserialize(JsonElement element, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
            String date = element.getAsString();

            if (date.toUpperCase().contains("NULL")) {
                return new Date();
            } else {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss");

                try {
                    Date rawDate = formatter.parse(date);
                    Date res = new Date();
                    res.setTime(rawDate.getTime());
                    return res;
                } catch (ParseException e) {
                    return null;
                }
            }
        }
    }

    public class MessageDeserializer implements JsonDeserializer<Message> {

        @Override
        public Message deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            JsonObject jsonObj = json.getAsJsonObject();
            System.out.println("...message type... " + Messages.convertSosMessage(jsonObj).getClass().getCanonicalName());
            return Messages.convertSosMessage(jsonObj);
        }
    }

    public class DialogEventInfo {
        public int layoutId;
        public int viewId;
        public Object data;
    }
}
