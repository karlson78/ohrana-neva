package ru.ohrana_neva.client.fragments;

import android.app.Dialog;
import android.content.Context;
import android.widget.TextView;

import ru.ohrana_neva.client.R;

public class DownloadProgressFragment extends BaseDialogFragment {
    private static int percent;

    public void setProgress(int value) {
        if (getDialog() != null) {
            percent = value;
            ((TextView) getDialog().findViewById(R.id.progress_text)).setText(String.format(getDialog().getContext().getResources().getString(R.string.download_progress), percent));
        }
    }

    public Dialog prepareDialog(final Context context, final int layoutId) {
        final Dialog dialog = super.prepareDialog(context, layoutId);
        setProgress(0);

        return dialog;
    }

    @Override
    public void refreshOnConfigChanged(Context context) {
        super.refreshOnConfigChanged(context);
        setProgress(percent);
    }
}
