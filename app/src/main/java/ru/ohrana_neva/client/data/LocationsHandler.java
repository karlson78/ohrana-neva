package ru.ohrana_neva.client.data;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;

import java.lang.ref.WeakReference;

public class LocationsHandler implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {
    public static long notify_interval = 10000;

    private WeakReference<OnGoogleApiConnectedListener> listener;

    public OnGoogleApiConnectedListener getListener() {
        return (listener == null) ? null : listener.get();
    }

    public void setListener(OnGoogleApiConnectedListener listener) {
        this.listener = new WeakReference<>(listener);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (listener != null && listener.get() instanceof AppCompatActivity) {
            boolean result = true;
            if (ActivityCompat.checkSelfPermission((AppCompatActivity) listener.get(),
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && (ActivityCompat.checkSelfPermission((AppCompatActivity) listener.get(),
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED))
            {
                result = false;
            }
            listener.get().onGoogleApiConnected(result);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        LocationsRecorder.getInstance().addLocation(location);
        if (listener != null && listener.get() instanceof AppCompatActivity) {
            listener.get().handleLocationChanged(location);
        }
    }

    public interface OnGoogleApiConnectedListener {
        void onGoogleApiConnected(boolean result);

        void handleLocationChanged(Location location);
    }
}
