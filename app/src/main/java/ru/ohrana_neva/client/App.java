package ru.ohrana_neva.client;

import android.app.Application;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.os.Build;
import android.os.PowerManager;

public class App extends Application {
    @Override
    public void onTrimMemory(final int level) {
        super.onTrimMemory(level);
        if (MainActivity.getInstance() != null) {
            MainActivity.getInstance().clearOSMcache();
            System.gc();
            if (level == ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN) {
                PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);

                boolean screenOn;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    screenOn = pm.isInteractive();
                } else {
                    screenOn = pm.isScreenOn();
                }

                if (screenOn) {
                    MainActivity.getInstance().setInBackground(true);
                }
            }
        }
    }
}
