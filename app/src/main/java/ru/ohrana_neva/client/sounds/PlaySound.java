package ru.ohrana_neva.client.sounds;

import android.app.Activity;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.ToneGenerator;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.text.TextUtils;

import java.io.File;
import java.io.IOException;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.Timer;
import java.util.TimerTask;

import ru.ohrana_neva.client.Consts;

public class PlaySound {
    private static volatile PlaySound instance = null;
    private WeakReference<Context> contextRef = null;
    private volatile SoftReference<MediaPlayer> player = null;
    private volatile SoftReference<ToneGenerator> toneG = null;
    private String dataPath = "";
    private volatile boolean fPlaying = false;
    private volatile boolean fStopped = false;
    private volatile Object lock;
    private volatile Timer mTimer = null;

    public static PlaySound getInstance(Context context) {
        PlaySound localRef = instance;
        if (localRef == null) {
            synchronized (PlaySound.class) {
                localRef = new PlaySound();
                localRef.contextRef = new WeakReference<Context>(context);
                localRef.lock = new Object();
                instance = localRef;
            }
        }
        return localRef;
    }

    public static void clear() {
        PlaySound localRef = instance;
        if (localRef == null) return;
        if (localRef.contextRef != null) localRef.contextRef.clear();
        localRef.contextRef = null;
        if (localRef.player != null && localRef.player.get() != null) {
            localRef.player.get().release();
            localRef.player.clear();
            localRef.player = null;
        }
        if (localRef.toneG != null && localRef.toneG.get() != null) {
            localRef.toneG.get().release();
            localRef.toneG.clear();
            localRef.toneG = null;
        }
        localRef.dataPath = "";
        localRef.fStopped = false;
        localRef.fPlaying = false;
        instance = null;
    }

    public String getAudioPath(Uri uri) {
        String[] data = {MediaStore.Audio.Media.DATA};
        CursorLoader loader = new CursorLoader(contextRef.get().getApplicationContext(), uri, data, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public boolean isPlaying() {
        return fPlaying;
    }

    public void play(String path) {
        synchronized (lock) {
            if (!TextUtils.isEmpty(path)) {
                boolean fCreate = false;
                if (player != null && player.get() == null) {
                    fCreate = true;
                } else {
                    if (path.compareTo(dataPath) != 0) {
                        if (player != null) player.get().release();
                        dataPath = path;
                        fCreate = true;
                    }
                }
                try {
                    if (fCreate) {
                        File file = new File(dataPath);
                        MediaPlayer mediaPlayer = MediaPlayer.create(contextRef.get(), Uri.fromFile(file));
                        mediaPlayer.setLooping(true);
                        if (player != null) player.clear();
                        player = new SoftReference<MediaPlayer>(mediaPlayer);
                        fStopped = false;
                    } else {
                        if (fStopped) {
                            player.get().prepare();
                            fStopped = false;
                        }
                    }
                    player.get().start();
                    fPlaying = true;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                fPlaying = true;
                fStopped = false;
                AudioManager am = (AudioManager) contextRef.get().getSystemService(Context.AUDIO_SERVICE);
                //if (am.getRingerMode() != AudioManager.RINGER_MODE_SILENT && am.getRingerMode() != AudioManager.RINGER_MODE_VIBRATE) {
                    toneG = new SoftReference<ToneGenerator>(new ToneGenerator(AudioManager.STREAM_ALARM, 100));
                    mTimer = new Timer();
                    mTimer.schedule(new TimerTaskToPlayAlarm(), 5, 1500);
                //}
            }
        }
    }

    public void stop() {
        synchronized (lock) {
            if (player != null && player.get() != null) {
                player.get().stop();
                fStopped = true;
                fPlaying = false;
            } else
            if (toneG != null && toneG.get() != null) {
                Handler handler = new Handler(Looper.getMainLooper());
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (toneG != null && toneG.get() != null) {
                            toneG.get().release();
                            toneG = null;
                            if (mTimer != null) {
                                mTimer.cancel();
                                mTimer = null;
                            }
                        }
                        fStopped = true;
                        fPlaying = false;
                    }
                }, 100);
            }
        }
    }

    public void openGalleryForAudio() {
        Intent videoIntent = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
        ((Activity) contextRef.get()).startActivityForResult(Intent.createChooser(videoIntent, "Select Audio"), Consts.AUDIO_REQUEST);
    }

    private class TimerTaskToPlayAlarm extends TimerTask {
        @Override
        public void run() {
            if (fPlaying && toneG != null && toneG.get() != null) {
                synchronized (lock) {
                    try {
                        toneG.get().startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 800);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
