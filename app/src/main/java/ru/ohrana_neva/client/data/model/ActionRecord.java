package ru.ohrana_neva.client.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ActionRecord {
    @SerializedName("id")
    public String id;
    @SerializedName("text")
    public String text;
    @SerializedName("user")
    public ActionUser user;
    @SerializedName("created_timestamp")
    public long created_timestamp;

    public static class ActionUser {
        @SerializedName("id")
        public String id;
        @SerializedName("username")
        public String username;
    }

    public static class ActionsResponse {
        @SerializedName("items")
        public List<ActionRecord> items;
        @SerializedName("count")
        public int count;
    }
}
