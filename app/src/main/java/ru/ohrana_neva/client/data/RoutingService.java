package ru.ohrana_neva.client.data;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import ru.ohrana_neva.client.data.model.CityGuideNearestAddrResponse;
import ru.ohrana_neva.client.data.model.CityGuideRouteResponse;

public interface RoutingService {
    @Headers({
            "Content-type: application/json"
    })
    @POST("route")
    Observable<CityGuideRouteResponse> getRoute(@Body String params);

    @Headers({
            "Content-type: application/json"
    })
    @POST("search/nearestaddress")
    Observable<CityGuideNearestAddrResponse> getNearestAddr(@Body String params);
}
