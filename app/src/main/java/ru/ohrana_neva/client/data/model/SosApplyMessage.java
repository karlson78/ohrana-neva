package ru.ohrana_neva.client.data.model;

public class SosApplyMessage extends Message {
    public boolean result;
    public String description;
    public String id;
    public double lat;
    public double lon;
}
