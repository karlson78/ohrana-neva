package ru.ohrana_neva.client.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import ru.ohrana_neva.client.R;
import ru.ohrana_neva.client.SharedPreferencesHelper;
import ru.ohrana_neva.client.data.ApiClient;

public class LoginFragment extends BaseDialogFragment {
    public Dialog prepareDialog(final Context context, final int layoutId) {
        final Dialog dialog = super.prepareDialog(context, layoutId);

        Button btnOk = (Button) dialog.findViewById(R.id.btn_ok);
        btnOk.setOnClickListener(v -> {
            ApiClient.getInstance(context).dialogEvent(layoutId, v.getId(), new UserInfo(((EditText) dialog.findViewById(R.id.et_login)).getText().toString(), ((EditText) dialog.findViewById(R.id.et_password)).getText().toString()));
            dialog.dismiss();
        });

        return dialog;
    }

    public void showDialog(Context context) {
        final Dialog dialog = prepareDialog(context, R.layout.dialogfragment_login);
        ((EditText) dialog.findViewById(R.id.et_login)).setText(SharedPreferencesHelper.getUsername(context));
        ((EditText) dialog.findViewById(R.id.et_password)).setText(SharedPreferencesHelper.getPassword(context));
        dialog.show();
    }

    public class UserInfo {
        public String name;
        public String password;

        public UserInfo(String name, String password) {
            this.name = name;
            this.password = password;
        }
    }

    @Override
    public void refreshOnConfigChanged(Context context) {
        super.refreshOnConfigChanged(context);
        showDialog(context);
    }
}
