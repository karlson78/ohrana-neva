package ru.ohrana_neva.client.versionupdater;

public interface DownloadProgressListener {
    void update(long bytesRead, long contentLength, boolean done);
}