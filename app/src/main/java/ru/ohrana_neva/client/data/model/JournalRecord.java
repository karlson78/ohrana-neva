package ru.ohrana_neva.client.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class JournalRecord {
    @SerializedName("id")
    public String id;
    @SerializedName("action")
    public String action;
    @SerializedName("operator_id")
    public String operator_id;
    @SerializedName("operator_name")
    public String operator_name;
    @SerializedName("brigade_id")
    public String brigade_id;
    @SerializedName("brigade_name")
    public String brigade_name;
    @SerializedName("created")
    public Date created;
    @SerializedName("created_timestamp")
    public long created_timestamp;
    @SerializedName("result")
    public String result;
    @SerializedName("params")
    public Params params;

    public class Params {
        @SerializedName("lat")
        public double lat;
        @SerializedName("lon")
        public double lon;
        @SerializedName("address")
        public String address;
        @SerializedName("name")
        public String name;
        @SerializedName("number")
        public String number;
        @SerializedName("pult_id")
        public String pult_id;
    }
}
