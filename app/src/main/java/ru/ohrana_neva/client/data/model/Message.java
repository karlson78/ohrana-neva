package ru.ohrana_neva.client.data.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Message implements Parcelable {
    public String action;

    public Message() {}

    protected Message(Parcel in) {
        action = in.readString();
    }

    public static final Creator<Message> CREATOR = new Creator<Message>() {
        @Override
        public Message createFromParcel(Parcel in) {
            return new Message(in);
        }

        @Override
        public Message[] newArray(int size) {
            return new Message[size];
        }
    };

    public String getHeader() {
        return "";
    };

    public String getText() {
        return "";
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(action);
    }
}
