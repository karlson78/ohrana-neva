package ru.ohrana_neva.client.data.model;

import android.os.Parcel;

public class SosMessage extends Message {
    public double lat;
    public double lon;
    public String id;
    public String address;

    public SosMessage() {}

    protected SosMessage(Parcel in) {
        action = in.readString();
        lat = in.readDouble();
        lon = in.readDouble();
        id = in.readString();
        address = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(action);
        dest.writeDouble(lat);
        dest.writeDouble(lon);
        dest.writeString(id);
        dest.writeString(address);
    }

    public static final Creator<SosMessage> CREATOR = new Creator<SosMessage>() {
        @Override
        public SosMessage createFromParcel(Parcel in) {
            return new SosMessage(in);
        }

        @Override
        public SosMessage[] newArray(int size) {
            return new SosMessage[size];
        }
    };

    @Override
    public String getText() {
        return this.address;
    }
}
